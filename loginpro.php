<?php 
session_start();
include 'db/dbConnection.php';

$name = $mysqli->escape_string($_POST['name']);

$result = $mysqli->query("SELECT * FROM user_tbl WHERE user_name='$name'");

if ( $result->num_rows == 0 ){ // User doesn't exist
    $_SESSION['message'] = "User doesn't exist!";
    		header("location:error.php");
  
  }
  else { // User exists
    $user = $result->fetch_assoc();
    
    if (password_verify($_POST['password'],$user['user_pwd'])) {
        
        $_SESSION['user_name'] = $user['user_name'];
        $_SESSION['user_type'] = $user['user_type'];
        $_SESSION['user_id'] = $user['user_id'];

        $_SESSION['user1'] = $user['user1'];
        $_SESSION['customer'] = $user['customer'];
        $_SESSION['suppliers'] = $user['suppliers'];
        $_SESSION['jobCat'] = $user['jobCat'];
        $_SESSION['job'] = $user['job'];
        $_SESSION['labour'] = $user['labour'];
        $_SESSION['labourPack'] = $user['labourPack'];
        $_SESSION['grn'] = $user['grn'];
        $_SESSION['release1'] = $user['release1'];
        $_SESSION['maReturn'] = $user['maReturn'];
        $_SESSION['invSett'] = $user['invSett'];
        $_SESSION['tax'] = $user['tax'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['category'] = $user['category'];
        $_SESSION['subCate'] = $user['subCate'];
        $_SESSION['brand'] = $user['brand'];
        $_SESSION['products'] = $user['products'];
        $_SESSION['quote'] = $user['quote'];
        $_SESSION['reQuote'] = $user['reQuote'];
        $_SESSION['invoice'] = $user['invoice'];
        $_SESSION['reInvoice'] = $user['reInvoice'];
        $_SESSION['stockRepo'] = $user['stockRepo'];
        $_SESSION['reProReport'] = $user['reProReport'];
        $_SESSION['matReReport'] = $user['matReReport'];
        $_SESSION['proReport'] = $user['proReport'];
        $_SESSION['quoteReport'] = $user['quoteReport'];
        $_SESSION['invReport'] = $user['invReport'];
        $_SESSION['payReport'] = $user['payReport'];

		header("location:index.php"); 

      }

    else {
        $_SESSION['message'] = "You have entered wrong password, try again!";
        	header("location: error.php");
        // echo "<script type='text/javascript'>goerror()</script>";
    }
}



?>
