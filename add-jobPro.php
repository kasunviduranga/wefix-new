<?php
include 'db/dbConnection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Forms - Atlantis Lite Bootstrap 4 Admin Dashboard</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                    "simple-line-icons"
                ],
                urls: ['assets/css/fonts.min.css']
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <!-- CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="assets/css/demo.css">
</head>

<body>
    <div class="wrapper">
        <!-- Navbar Header -->
        <?php include('header.php'); ?>
        <!-- End Navbar -->
        <!-- Sidebar -->
        <?php include('sidebar.php'); ?>
        <!-- End Sidebar -->
        <div class="main-panel">
            <div class="content">

                <?php

                $statusMsg = '';
                $date = date("m/d/Y");
                $user = $_SESSION['user_id'];

                $select9 = "SELECT MAX(file_id) AS max_id FROM jobfile_tbl";
                $result9 = mysqli_query($connection, $select9);
                $dataRow9 = mysqli_fetch_array($result9);
                $dataRow9 = ++$dataRow9['max_id'];
                $fileId = $dataRow9;

                // File upload path
                $targetDir = "uploads/";
                $fileName = basename($_FILES["file"]["name"]);
                $targetFilePath = $targetDir . $fileName;
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);

                $allowTypes = array('csv');

                if (in_array($fileType, $allowTypes)) {
                    // Upload file to server

                    if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)) {
                        // Insert image file name into database

                        $sql = "INSERT INTO jobfile_tbl(`file_id`,`file_name`,`added_date`,`added_user`)VALUES(
                            $fileId,
                            '$fileName',
                            '$date',
                            $user)";
                        $statusMsg = "Added Success.";
                        if ($connection->query($sql) === true) {

                            $f_pointer = fopen("uploads/$fileName", "r"); // file pointer
                            $first_line = "T";
                            $array = array();
                            $array2 = array();

                            while (!feof($f_pointer)) {
                                $ar = fgetcsv($f_pointer);
                                if ($first_line <> 'T') {
                                    // customer insert 
                                    $sql2 = "INSERT INTO customer_tbl(`cus_id`,`cus_name`,`cus_phone`,`cus_email`,`cus_custom_feild`,`added_user`,`added_date`,`address`,`credit`,`company`)VALUES(
                                            0,
                                            '$ar[2]',
                                            '12',
                                            '12',
                                            '12',
                                            1,
                                            '12',
                                            '12',
                                            '12',
                                            '12')";
                                    
                                    //job insert for customer
                                    $sql1 = "INSERT INTO job_tbl(`job_id`,`job_no`,`customer_name`,`customer_category`,`service_type`,`job_type`,`job_status`,`create_on`,`assign_on`,`dispatch_on`,`complete_on`,`date`,`job_file`,`send_quote`,`send_invoice`)VALUES(
                                            0,
                                            '$ar[1]',
                                            '$ar[2]',
                                            '$ar[3]',
                                            '$ar[4]',
                                            '$ar[5]',
                                            'Pending',
                                            '$ar[7]',
                                            '$ar[8]',
                                            '$ar[9]',
                                            '$ar[10]',
                                            '$ar[0]',
                                            $fileId,
                                            0,
                                            0)";
                                    
                                    // push multiple queries in array for customer and job vice
                                    array_push($array, $sql1);
                                    array_push($array2, $sql2);
                                }
                                $first_line = 'F';
                            }
                            // get length array
                            $length = count($array);
                            // loop and  insert job
                            for ($i = 0; $i < $length - 1; $i++) {
                                $query4 = $array[$i];
                                if ($connection->query($query4) === true) {
                                }
                            }

                            // loop and insert customer
                            for ($j = 0; $j < $length - 1; $j++) {
                                $query5 = $array2[$j];
                                if ($connection->query($query5) === true) {
                                }
                            }?>

                            <script>
                                //== Class definition
                                var SweetAlert2Demo = function() {

                                    //== Demos
                                    var initDemos = function() {
                                        //== Sweetalert Demo 1
                                        swal({
                                            icon: "success",
                                            title: 'job Added Success !',
                                            type: 'success',
                                            buttons: {
                                                confirm: {
                                                    text: 'OK',
                                                    className: 'btn btn-success'
                                                }
                                            }
                                        }).then((Delete) => {
                                            if (Delete) {
                                                window.location.replace("list-job.php");
                                            } else {
                                                window.location.replace("list-job.php");
                                            }
                                        });
                                    };

                                    return {
                                        //== Init
                                        init: function() {
                                            initDemos();
                                        },
                                    };
                                }();

                                //== Class Initialization
                                jQuery(document).ready(function() {
                                    SweetAlert2Demo.init();
                                });
                            </script>
                        <?php } else { ?>
                            <script>
                                //== Class definition
                                var SweetAlert2Demo = function() {

                                    //== Demos
                                    var initDemos = function() {
                                        //== Sweetalert Demo 1
                                        swal({
                                            icon: "error",
                                            title: 'job Added not Success !',
                                            type: 'error',
                                            buttons: {
                                                confirm: {
                                                    text: 'OK',
                                                    className: 'btn btn-danger'
                                                }
                                            }
                                        }).then((Delete) => {
                                            if (Delete) {
                                                window.location.replace("add-job.php");
                                            } else {
                                                window.location.replace("add-job.php");
                                            }
                                        });
                                    };

                                    return {
                                        //== Init
                                        init: function() {
                                            initDemos();
                                        },
                                    };
                                }();

                                //== Class Initialization
                                jQuery(document).ready(function() {
                                    SweetAlert2Demo.init();
                                });

                                // window.location.replace("list-customers.php");
                            </script>
                        <?php } ?>
                    <?php } else {
                        $statusMsg = "Sorry, there was an error sending your file."; ?>
                        <script>
                            //== Class definition
                            var SweetAlert2Demo = function() {

                                //== Demos
                                var initDemos = function() {
                                    //== Sweetalert Demo 1
                                    swal({
                                        icon: "error",
                                        title: 'Sorry, there was an error sending your file.',
                                        type: 'error',
                                        buttons: {
                                            confirm: {
                                                text: 'OK',
                                                className: 'btn btn-danger'
                                            }
                                        }
                                    }).then((Delete) => {
                                        if (Delete) {
                                            window.location.replace("add-job.php");
                                        } else {
                                            window.location.replace("add-job.php");
                                        }
                                    });
                                };

                                return {
                                    //== Init
                                    init: function() {
                                        initDemos();
                                    },
                                };
                            }();

                            //== Class Initialization
                            jQuery(document).ready(function() {
                                SweetAlert2Demo.init();
                            });

                            // window.location.replace("list-customers.php");
                        </script>
                    <?php }
                } else {
                    $statusMsg = 'Sorry, only csv files allowed to upload.'; ?>
                    <script>
                        //== Class definition
                        var SweetAlert2Demo = function() {

                            //== Demos
                            var initDemos = function() {
                                //== Sweetalert Demo 1
                                swal({
                                    icon: "error",
                                    title: 'Sorry, only csv files allowed to upload.',
                                    type: 'error',
                                    buttons: {
                                        confirm: {
                                            text: 'OK',
                                            className: 'btn btn-danger'
                                        }
                                    }
                                }).then((Delete) => {
                                    if (Delete) {
                                        window.location.replace("add-job.php");
                                    } else {
                                        window.location.replace("add-job.php");
                                    }
                                });
                            };

                            return {
                                //== Init
                                init: function() {
                                    initDemos();
                                },
                            };
                        }();

                        //== Class Initialization
                        jQuery(document).ready(function() {
                            SweetAlert2Demo.init();
                        });

                        // window.location.replace("list-customers.php");
                    </script>
                <?php } ?>
            </div>
            <!-- footer -->
            <?php include('footer.php'); ?>
            <!-- End footer -->
        </div>

        <!-- Custom template | don't include it in your project! -->
        <?php include('rightSidebar.php'); ?>
        <!-- End Custom template -->
    </div>

    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <!-- jQuery UI -->
    <script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <!-- Atlantis JS -->
    <script src="assets/js/atlantis.min.js"></script>
    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="assets/js/setting-demo2.js"></script>
    <!-- Sweet Alert -->
    <script src="assets/js/plugin/sweetalert/sweetalert.min.js"></script>
</body>

</html>
