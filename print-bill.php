<!DOCTYPE html>
<html lang="en" id="printElement">

<?php 
session_start();

	if (!isset($_SESSION['user_name'])){
		header('Location: login.php?err=1');
	}
?>

<?php include 'db/dbConnection.php'; ?>

<?php
$id = $_GET['id'];
?>

<head>
    <meta charset="utf-8">
    <title>Quotation | WEFIX</title>
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>
    <link rel="stylesheet" href="assets/css/bill.css" media="all" />
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <style>
    /* override styles when printing */
    @media print {
        #openWin {
            display: none;
        }

        #backbtn {
            display: none;
        }

        .tox-menubar {
            display: none !important;
        }

        .tox-toolbar {
            display: none !important;
        }

        .tox-statusbar {
            display: none !important;
        }

        #tinymce {
            overflow-y: hidden !important;
        }

        .tox {
            border: none !important;
        }

        .tox-tinymce {
            border: none !important;
        }

        .tox-edit-area {
            border: none !important;
        }
    }

    .tox-statusbar__branding {
        display: none !important;
    }
    </style>
</head>

<button type="button" id="openWin" class="btn btn-icon btn-round" title=""
    style="position:fixed;margin:auto; bottom:250px; right:10px; width:60px; height:60px; background-color:#5C55BF; border:none; cursor:pointer;"
    onclick="window.print();return false;">
    <i class="fas fa-print" style="font-size:180%; color:white;"></i>
</button>
<button type="button" class="btn btn-icon btn-round" id="backbtn"
    style="position:fixed;margin:auto; bottom:160px; right:10px; width:60px; height:60px; background-color:#5C55BF; border:none; cursor:pointer;"
    onclick="goBack()">
    <i class="fas fa-arrow-left" style="font-size:200%; color:white;"></i>
</button>

<body style="background-color: white;">

    <?php
    $total = 0;

    $sql = mysqli_query($connection, "SELECT * FROM quatation_tbl WHERE  quate_id = '$id'");
    $res = mysqli_fetch_array($sql);
    
        $added_user = $res['added_user'];
        $added_date = $res['added_date'];
        $added_time = $res['added_time'];
        $disc = $res['quote_disc'];
        $advance = $res['quote_advance'];
        $quote_no = $res['quote_no'];
        $addTax = $res['add_tax'];
        $cusId = $res['quote_cusID'];
        
    $sql = mysqli_query($connection, "SELECT * FROM customer_tbl WHERE cus_id = '$cusId'");
    $res = mysqli_fetch_array($sql);
        
        $customer = $res['cus_name'];
        $custAddress = $res['address'];
        $custEmail = $res['cus_email'];
        
    $x = 1;
    ?>

    <?php
    $sql1 = mysqli_query($connection, "SELECT * FROM invo_quotesetting_tbl WHERE id = 1");
    $res1 = mysqli_fetch_array($sql1);
    
    $name = $res1['name'];
    $number = $res1['number'];
    $email = $res1['email'];
    $image = $res1['image'];
    $address = $res1['address'];
    $footer = $res1['footer'];
    ?>

    <header class="clearfix" style="background-color: white;">
        <div id="logo">
            <img src="image/<?php echo $image ?>">
        </div>
        <div id="company">
            <h2 class="name"><?php echo $name ?></h2>
            <div><?php echo $address ?></div>
            <div><?php echo $number ?></div>
            <div><a href="mailto:<?php echo $email ?>" target="_new"><?php echo $email ?></a></div>
        </div>
        </div>
    </header>
    <main style="background-color: white;">
        <div id="details" class="clearfix">
            <div id="client">
                <div class="to">Received From:</div>
                <h2 class="name"><?php echo $customer ?></h2>
                <div class="address"><?php echo $custAddress ?></div>
                <div class="email"><a href="mailto:<?php echo $custEmail ?>" target="_new"><?php echo $custEmail ?></a>
                </div>
            </div>
            <div id="invoice">
                <h1>RECEIPT</h1>
                <div class="date">Receipt Date : <?php echo $added_date ?></div>
                <div class="date">Quote Number : WEFIX/R/<?php echo $id ?></div>
                <div class="date">Quote Number : <?php echo $quote_no ?></div>
                <!-- <div class="date">Valid Date: 30/06/2014</div> -->
            </div>
        </div>
        <table border="0" cellspacing="0" cellpadding="0" id="table1">
            <thead>
                <tr>
                    <th class="unit" style="border: 1px solid black;">No</th>
                    <th class="desc" style="border: 1px solid black;">Description</th>
                    <th class="unit" style="border: 1px solid black;">Unit</th>
                    <!-- <th class="qty" style="border: 1px solid black;">Qty</th> -->
                    <th class="unit" style="border: 1px solid black;">Cheque Number </th>
                    <th class="total" style="border: 1px solid black;">Amount (Rs)</th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td class="no" style="font-size: 13px;border: 0.5px solid black;">01</td>
                    <td class="desc" style="border: 0.5px solid black;">
                        <h3 style="font-size: 14px;">Advance Payment Received</h3>
                    </td>
                    <td class="unit" style="font-size: 13px;border: 0.5px solid black;">Cash/Cheq</td>
                    <!-- <td class="qty" style="border: 0.5px solid black;"></td> -->
                    <td class="unitPrice" style="border: 0.5px solid black;"><input type="text"
                            style="width:100%;border: none;"></td>
                    <td class="total" style="font-size: 16px;border: 0.5px solid black;">
                        <?php echo number_format($advance,2); ?>
                    </td>
                </tr>

            </tbody>

            <tfoot>
                <tr>
                    <!-- <td colspan="1" style="border-left: 0.5px solid black; border-bottom: 0.5px solid black;"></td> -->
                    <td colspan="3" style="border-bottom: 0.5px solid black;border-left: 0.5px solid black;"></td>
                    <td colspan="1" style="border-bottom: 0.5px solid black;">Total Payable Amount (Rs.) </td>
                    <td style="text-align:right; border: 0.5px solid black;"> <?php echo number_format($advance,2); ?>
                    </td>
                </tr>
            </tfoot>
        </table>
        <div id="thanks">
            <p>&nbsp;</p>
            <table style="border: none; border-color: white; width: 99.5244%; height: 46px;">
                <tbody>
                    <tr>
                        <td style="width: 55.2569%;"><span style="font-size: 12pt;">We Thank you for your Business with
                                us.........</span><br /><span style="font-size: 12pt;">WEFIX</span><br /><span
                                style="font-size: 12pt;">by</span><br /><span style="font-size: 12pt;">APS Lanka (pvt.)
                                Ltd.</span></td>
                        <td style="width: 44.7431%; text-align: right;"><span
                                style="font-size: 12pt;">...............................................</span><br /><span
                                style="font-size: 12pt;"><strong>Received By</strong></span></td>
                    </tr>
                </tbody>
            </table>
            <p>&nbsp;</p>
            <p style="text-align: center;"><img src="blob:http://localhost:8080/23e2558c-db02-4879-9b56-38fc0c61fee3"
                    width="106" height="45" /></p>
            <p style="text-align: center;"><strong>WEFIX (PVT) LTD</strong><br />119/1, New Kandy Road, Kothalawala,
                Kaduwela, Sri Lanka<br />+94 112 548808 | +94 112 548724, <a href="mailto:info@apslanka.com,"
                    target="_blank" rel="noopener">info@apslanka.com,</a> <a href="http://www.apslanka.com"
                    target="_blank" rel="noopener">www.apslanka.com</a></p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
    </main>

    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <script>
    function goBack() {
        window.location = 'list-quotation.php';
    }
    </script>

    <!-- partial -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.5/tinymce.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
</body>

</html>