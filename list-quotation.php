<?php 
include 'db/dbConnection.php';  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Wefix</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="assets/css/demo.css">
</head>

<body>
    <div class="wrapper">
        <!-- Navbar Header -->
        <?php include('header.php');?>
        <!-- End Navbar -->
        <!-- Sidebar -->
        <?php include('sidebar.php');?>
        <!-- End Sidebar -->
        <div class="main-panel">
            <div class="content">
                <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">List Quotation</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="index.php">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">List Quotation</a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">List Quotation</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="d-flex align-items-center">
                                        <a class=" ml-auto" href="quotation.php">
                                            <button class="btn btn-primary btn-round" data-toggle="modal"
                                                data-target="#">
                                                <i class="fa fa-plus"></i>
                                                New Quotation
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="example" class="display table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Quote No</th>
                                                    <th>Job No</th>
                                                    <th>Customer</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Quote No</th>
                                                    <th>Job No</th>
                                                    <th>Customer</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php

											$sql="SELECT * FROM quatation_tbl limit 0, 11";
											$result = mysqli_query($connection,$sql);

											while($dataRow=mysqli_fetch_assoc($result)){
                                                $revise = $dataRow['revise_quote'];
                                                $reAddQt = $dataRow['reQuote'];
                                                $accept_qte = $dataRow['accept_qte'];
                                                

                                                $sql1 = mysqli_query($connection, "SELECT * FROM customer_tbl WHERE cus_id = '$dataRow[quote_cusID]'");
                                                $res1 = mysqli_fetch_array($sql1);

                                                $sql2 = mysqli_query($connection, "SELECT * FROM job_tbl WHERE job_id = '$dataRow[quote_job]'");
                                                $res2 = mysqli_fetch_array($sql2);

                                                echo "<tr>";    
                                                echo "<td >".$dataRow['quote_no']."</td>";
                                                echo "<td >".$res2['job_no']."</td>";
                                                echo "<td >".$res1['cus_name']."</td>";
                                                echo "<td >".$dataRow['added_date']."</td>";
                                                echo "<td>
														<div class=\"form-button-action\">

                                                            <a style='width:20px; height:20px; text-align:center; margin-left: 0px;' href='quotation-details.php?id=$dataRow[quate_id]'>

                                                                <i style=\"font-size: 25px;\" data-toggle=\"tooltip\" title=\"View Details\" data-original-title=\"View Details\" class=\"fa fa-eye\"></i>

                                                            </a>";

                                                if ($accept_qte == 1) {
                                                            
                                                    echo "<a style='width:20px; height:20px; text-align:center; margin-left: 25px;' href='move-invoice.php?id=$dataRow[quate_id]'>

                                                        <i style=\"font-size: 22px;\" data-toggle=\"tooltip\" title=\"Move to invoice\" data-original-title=\"Move to invoice\" class=\"fas fa-file-export\"></i>
                
                                                    </a>";
                                                }

                                                if ($accept_qte == 0) {
                                                    echo "<a style='width:20px; height:20px; text-align:center; margin-left: 25px;;' href='#' onclick='addDisID($dataRow[quate_id])'>

                                                        <i style=\"font-size: 22px;\" data-toggle=\"tooltip\" title=\"Quotation Accept\" data-original-title=\"Quotation Accept\" class=\"fas fa-user-check\"></i>

                                                    </a>";
                                                }

                                                if ($reAddQt == 0 && $accept_qte == 0) {

                                                    echo "<a style='width:20px; height:20px; text-align:center; margin-left: 25px;' href='quotation-reAdd.php?id=$dataRow[quate_id]'>

														<i style=\"font-size: 22px;\" data-toggle=\"tooltip\" title=\"Re Add\" data-original-title=\"Re Add\" class=\"fas fa-redo-alt\"></i>

                                                    </a>";
                                                }

                                                if ($revise == 0 && $accept_qte == 0) {
                                                            
                                                echo "<a style='width:20px; height:20px; text-align:center; margin-left: 25px;' href='quotation-revise.php?id=$dataRow[quate_id]'>

													    <i style=\"font-size: 25px;\" data-toggle=\"tooltip\" title=\"Revise\" data-original-title=\"Revise\" class=\"fas fa-arrow-circle-left\"></i>

                                                    </a>";
                                                }     
                                                echo "</div> </td>";
											    echo "</tr>";
											}
											?>
                                                <script>
                                                    documen
                                                </script>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
            <?php include('footer.php');?>
            <!-- End footer -->

            <!-- Add discount Modal -->
            <div class="modal fade" id="addDiscount" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form action="quotation-accept.php" method="post" enctype="multipart/form-data">
                        <div class="modal-content">
                            <div class="modal-header no-bd">
                                <h4 class="modal-title">
                                    <span class="fw-mediumbold"> ACCEPT </span>
                                    <span class="fw-light"> QUOTATION </span>
                                </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" style="display:none;">
                                            <input type="text" class="form-control" id="txtAcceptID" name="txtAcceptID">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Advance Price</label>
                                            <input type="text" value="0" class="form-control" id="txtDiscount"
                                                name="txtDiscount">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer no-bd">
                                <button class="btn btn-primary" type="submit"> Accept </button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"> Close </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end Modal -->
        </div>

        <!-- Custom template | don't include it in your project! -->
        <?php include('rightSidebar.php');?>
        <!-- End Custom template -->
    </div>
    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <!-- jQuery UI -->
    <script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <!-- Datatables -->
    <script src="assets/js/plugin/datatables/datatables.min.js"></script>
    <!-- Atlantis JS -->
    <script src="assets/js/atlantis.min.js"></script>
    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="assets/js/setting-demo2.js"></script>

    <!-- Sweet Alert -->
    <script src="assets/js/plugin/sweetalert/sweetalert.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

    <script>
    function addDisID(id) {
        document.getElementById("txtAcceptID").value = id;
        $('#addDiscount').modal('show');
    }

    $(document).ready(function() {
        $('#example').DataTable({
            // dom: 'Bfrtip',
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ]
        });
    });
    </script>
</body>

</html>