<?php include 'db/dbConnection.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Wefix</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <!-- CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="assets/css/demo.css">
    <!-- select2 -->
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css'>
</head>

<body>
    <div class="wrapper sidebar_minimize">
        <!-- Navbar -->
        <?php include('header.php');?>
        <!-- End Navbar -->
        <!-- Sidebar -->
        <?php include('sidebar.php');?>
        <!-- End Sidebar -->
        <style>
        .select2 {
            width: 100% !important;
        }

        .select2-selection {
            border-color: #ebedf2 !important;
        }

        .select2.select2-container {
            width: 100% !important;
        }

        .select2.select2-container .select2-selection {
            border: 1px solid #ccc;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            height: 34px;
            margin-bottom: 15px;
            outline: none;
            transition: all 0.15s ease-in-out;
        }

        .select2.select2-container .select2-selection .select2-selection__rendered {
            color: #333;
            line-height: 32px;
            padding-right: 33px;
        }

        .select2.select2-container .select2-selection .select2-selection__arrow {
            background: #f8f8f8;
            border-left: 1px solid #ccc;
            -webkit-border-radius: 0 3px 3px 0;
            -moz-border-radius: 0 3px 3px 0;
            border-radius: 0 3px 3px 0;
            height: 32px;
            width: 33px;
        }

        .select2.select2-container.select2-container--open .select2-selection.select2-selection--single {
            background: #f8f8f8;
        }

        .select2.select2-container.select2-container--open .select2-selection.select2-selection--single .select2-selection__arrow {
            -webkit-border-radius: 0 3px 0 0;
            -moz-border-radius: 0 3px 0 0;
            border-radius: 0 3px 0 0;
        }

        .select2.select2-container.select2-container--open .select2-selection.select2-selection--multiple {
            border: 1px solid #34495e;
        }

        .select2.select2-container.select2-container--focus .select2-selection {
            border: 1px solid #34495e;
        }

        .select2.select2-container .select2-selection--multiple {
            height: auto;
            min-height: 34px;
        }

        .select2.select2-container .select2-selection--multiple .select2-search--inline .select2-search__field {
            margin-top: 0;
            height: 32px;
        }

        .select2.select2-container .select2-selection--multiple .select2-selection__rendered {
            display: block;
            padding: 0 4px;
            line-height: 29px;
        }

        .select2.select2-container .select2-selection--multiple .select2-selection__choice {
            background-color: #f8f8f8;
            border: 1px solid #ccc;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            margin: 4px 4px 0 0;
            padding: 0 6px 0 22px;
            height: 24px;
            line-height: 24px;
            font-size: 12px;
            position: relative;
        }

        .select2.select2-container .select2-selection--multiple .select2-selection__choice .select2-selection__choice__remove {
            position: absolute;
            top: 0;
            left: 0;
            height: 22px;
            width: 22px;
            margin: 0;
            text-align: center;
            color: #e74c3c;
            font-weight: bold;
            font-size: 16px;
        }

        .select2-container .select2-dropdown {
            background: transparent;
            border: none;
            margin-top: -5px;
        }

        .select2-container .select2-dropdown .select2-search {
            padding: 0;
        }

        .select2-container .select2-dropdown .select2-search input {
            outline: none;
            border: 1px solid #34495e;
            border-bottom: none;
            padding: 4px 6px;
        }

        .select2-container .select2-dropdown .select2-results {
            padding: 0;
        }

        .select2-container .select2-dropdown .select2-results ul {
            background: #fff;
            border: 1px solid #34495e;
        }

        .select2-container .select2-dropdown .select2-results ul .select2-results__option--highlighted[aria-selected] {
            background-color: #3498db;
        }

        .big-drop {
            width: 600px !important;
        }
        </style>
        <div class="main-panel">
            <div class="content">
                <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">QUOTATION REPORT</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="index.php">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">Report</a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">Quotation Report</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <select class="form-control js-select2" id="jobtxt" onchange="searchByCus(this.value)">
                                                    <option disabled selected hidden >Search by Customer</option>
                                                    <?php
                                                    $sql = mysqli_query($connection,"SELECT * FROM customer_tbl");
                                                    $row = mysqli_num_rows($sql);
                                                    while ($row = mysqli_fetch_array($sql)){
                                                        echo "<option value='". $row['cus_id'] ."'>" .$row ['cus_name'] ." | ".$row ['cus_phone'] ." | ".$row ['cus_email']."</option>" ;
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="d-flex align-items-center">
                                        <a class=" ml-auto" href="quotation-report.php">
                                            <button class="btn btn-primary btn-round">ALL</button>
                                        </a>
									</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="example" class="display table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Customer Name</th>
                                                    <th>Discount</th>
                                                    <th>Advance</th>
                                                    <th>Accepted</th>
                                                    <th>Product Released</th>
                                                    <th>Total Price(Rs.)</th>
                                                    <th>Date</th>
                                                    <th>View Details</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <!-- <tr>
                                                    <th>NO</th>
                                                    <th>Customer Name</th>
                                                    <th>Discount</th>
                                                    <th>Advance</th>
                                                    <th>Accepted</th>
                                                    <th>Product Released</th>
                                                    <th>Total Price(Rs.)</th>
                                                    <th>Date</th>
                                                    <th>View Details</th>
                                                </tr> -->
                                            </tfoot>
                                            <tbody>
                                                <?php

                                            $sql = mysqli_query($connection, "SELECT * FROM tax_tbl WHERE id = '1'");
                                            $res = mysqli_fetch_array($sql);
                                            $tax = $res['tax'];

                                            if (!empty($_GET['id'])) {

                                                $id = $_GET['id'];
                                                $sql="SELECT * FROM quatation_tbl,customer_tbl WHERE  quatation_tbl.quote_cusID = $id AND customer_tbl.cus_id = quatation_tbl.quote_cusID";
                                                $result = mysqli_query($connection,$sql);
                                                
                                            } else {

                                                $sql="SELECT * FROM quatation_tbl,customer_tbl WHERE customer_tbl.cus_id = quatation_tbl.quote_cusID";
                                                $result = mysqli_query($connection,$sql);
                                                
                                            }

											while($dataRow=mysqli_fetch_assoc($result)){
                                                $total = 0.0;
                                                $id = $dataRow['quate_id'];

                                                $sql1="SELECT * From quate_details_tbl WHERE quate_id = '$id'";
                                                $result1 = mysqli_query($connection,$sql1);
			                                    while($dataRow1=mysqli_fetch_assoc($result1)){ 
                                                    $total += $dataRow1['totQty'] * $dataRow1['quote_price'];
                                                }

                                                $sql2="SELECT * FROM quate_lapack_tbl,labourpack_tbl WHERE quate_lapack_tbl.quateRef_id = '$id' AND labourpack_tbl.labourPack_id = quate_lapack_tbl.quitPack_id;";
                                                $result2 = mysqli_query($connection,$sql2);
			                                    while($dataRow2=mysqli_fetch_assoc($result2)){ 
                                                    $total += $dataRow2['labourPack_price'];
                                                }

                                                $sql3="SELECT * From quote_additional_tbl WHERE quote_id = '$id';";
                                                $result3 = mysqli_query($connection,$sql3);
			                                    while($dataRow3=mysqli_fetch_assoc($result3)){ 
                                                    $total += $dataRow3['additional_price'];
                                                }

                                                $total += ($total * $dataRow['quote_disc'] / 100);

                                                if ($dataRow['add_tax'] == 0) {
                                                    $total += ($total * $tax / 100);
                                                }

                                                echo "<tr>";
                                                echo "<td >".$dataRow['quote_no']."</td>";
                                                echo "<td >".$dataRow['cus_name']."</td>";
                                                echo "<td >".$dataRow['quote_disc']." % </td>";
                                                echo "<td >".$dataRow['quote_advance']."</td>";
                                                if ($dataRow['accept_qte'] == 1) {
                                                    echo "<td ><a style='width:20px; height:20px; text-align:center; margin-left: 25px;'>
                                                        <i style=\"font-size: 25px;\" class=\"fas fa-check\"></i>
                                                    </a></td>";
                                                } else {
                                                    echo "<td ><a style='width:20px; height:20px; text-align:center; margin-left: 25px;'>
                                                        <i style=\"font-size: 25px;\" class=\"fas fa-times\"></i>
                                                    </a></td>";
                                                }

                                                $sql1="SELECT * FROM quate_details_tbl WHERE quate_id = '$dataRow[quate_id]' ";
                                                    $result1 = mysqli_query($connection,$sql1);
                                                    
                                                $reQTY=0;
											    while($dataRow1=mysqli_fetch_assoc($result1)){
                                                    $reQTY += $dataRow1['releas_qty'];
                                                }

                                                if ($reQTY > 0) {
                                                    echo "<td ><a style='width:20px; height:20px; text-align:center; margin-left: 25px;'>
                                                        <i style=\"font-size: 25px;\" class=\"fas fa-times\"></i>
                                                    </a></td>";
                                                    
                                                    
                                                } else {
                                                    echo "<td ><a style='width:20px; height:20px; text-align:center; margin-left: 25px;'>
                                                        <i style=\"font-size: 25px;\" class=\"fas fa-check\"></i>
                                                    </a></td>";
                                                }
                                                echo "<td >".number_format($total,2)."</td>";
                                                echo "<td >".$dataRow['added_date']."</td>";
                                                echo "<td>
														<div class=\"form-button-action\">
															<a href='quotation-details.php?id=$dataRow[quate_id]'>
																<button type=\"button\" data-toggle=\"tooltip\" 		title=\"View Details\" class=\"btn btn-link btn-primary btn-lg\" data-original-title=\"Edit Task\">
																	<i class=\"fa fa-eye\"></i>
																</button>
                                                            </a>
                                                            <a href='release-details.php?id=$dataRow[quate_id]'>
																<button type=\"button\" data-toggle=\"tooltip\" 		title=\"Pro. Release Details\" class=\"btn btn-link btn-primary btn-lg\" data-original-title=\"Pro. Release Details\">
																	<i class=\"fas fa-cart-arrow-down\"></i>
																</button>
															</a>
														</div>
													</td>";
											    echo "</tr>";
											}
											?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
            <?php include('footer.php');?>
            <!-- End footer -->
        </div>

        <!-- Custom template | don't include it in your project! -->
        <?php include('rightSidebar.php');?>
        <!-- End Custom template -->
    </div>
    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <!-- jQuery UI -->
    <script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <!-- Datatables -->
    <script src="assets/js/plugin/datatables/datatables.min.js"></script>
    <!-- Atlantis JS -->
    <script src="assets/js/atlantis.min.js"></script>
    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="assets/js/setting-demo2.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

    <script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });

    function searchByCus(cusId) {
        window.location.href = "quotation-report.php?id="+cusId;
    }
    </script>
    <!-- select2 -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js'></script>
    <script>
    $(document).ready(function() {

        $(".js-select2").select2();

        $(".js-select2-multi").select2();

        $(".large").select2({
            dropdownCssClass: "big-drop",
        });

    });
    </script>

</body>

</html>