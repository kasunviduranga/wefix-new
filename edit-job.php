<?php include 'db/dbConnection.php'; ?>

<?php
$id = $_GET['id'];

$sql = mysqli_query($connection, "SELECT * FROM job_tbl WHERE job_id = '$id'");
      $res = mysqli_fetch_array($sql);

$job_no = $res['job_no'];
$customer_name = $res['customer_name'];
$customer_category = $res['customer_category'];
$service_type = $res['service_type'];
$job_type = $res['job_type'];
$job_status = $res['job_status'];
$create_on = $res['create_on'];
$assign_on = $res['assign_on'];
$dispatch_on = $res['dispatch_on'];
$complete_on = $res['complete_on'];
$date = $res['date'];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Edit Job | WEFIX</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="assets/css/demo.css">
</head>

<body>
    <div class="wrapper">
        <!-- Navbar Header -->
        <?php include('header.php');?>
        <!-- End Navbar -->
        <!-- Sidebar -->
        <?php include('sidebar.php');?>
        <!-- End Sidebar -->
        <div class="main-panel">
            <div class="content">
                <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">EDIT JOB</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="index.php">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">Job List</a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">Edit Job</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="edit-jobPro.php" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="edit-jobPro.php" method="post" enctype="multipart/form-data">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="">Job No</label>
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $job_no ?>" name="txt_no">
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $id ?>" name="txt_id" hidden>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Customer Name</label>
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $customer_name ?>" name="txt_name">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Customer Category</label>
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $customer_category ?>"
                                                                    name="txt_category">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Service Type</label>
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $service_type ?>"
                                                                    name="txt_serviceType">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Job Type</label>
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $job_type ?>" name="txt_jobType">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Job Status</label>
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $job_status ?>" name="txt_status">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Create On</label>
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $create_on ?>" name="txt_create">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Assign On</label>
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $assign_on ?>" name="txt_assign">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Dispatch On</label>
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $dispatch_on ?>"
                                                                    name="txt_dispatch">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Complete On</label>
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $complete_on ?>"
                                                                    name="txt_complete">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Date</label>
                                                                <input type="text" class="form-control"
                                                                    value="<?php echo $date ?>" name="txt_date">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-action">
                                                    <button class="btn btn-primary" type="submit">
                                                        <span class="btn-label">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                        Edit Job
                                                    </button>
                                                    <a href='list-job.php' class="btn btn-danger">
                                                        <span class="btn-label">
                                                            <i class="fa fa-times"></i>
                                                        </span>
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
            <?php include('footer.php');?>
            <!-- End footer -->
        </div>

        <!-- Custom template | don't include it in your project! -->
        <?php include('rightSidebar.php');?>
        <!-- End Custom template -->
    </div>
    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <!-- jQuery UI -->
    <script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <!-- Atlantis JS -->
    <script src="assets/js/atlantis.min.js"></script>
    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="assets/js/setting-demo2.js"></script>
</body>

</html>