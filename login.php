<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login | WEFIX</title>
    <!-- Meta-Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <!-- <meta name="keywords" content="Business Login Form a Responsive Web Template, Bootstrap Web Templates, Flat Web Templates, Android Compatible Web Template, Smartphone Compatible Web Template, Free Webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"> -->
    <script>
    addEventListener("load", function() {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
    </script>
    <!-- //Meta-Tags -->
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />

    <!-- css files -->
    <link href="assets/login/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/login/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!-- //css files -->

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- //google fonts -->

</head>

<body>

    <div class="signupform">
        <div class="container">
            <!-- main content -->
            <div class="agile_info">
                <div class="w3l_form">
                    <div class="left_grid_info">
                        <h1 style="color:#f5a442">WEFIX</h1>
                        <!-- <p>Donec dictum nisl nec mi lacinia, sed maximus tellus eleifend. Proin molestie cursus sapien ac eleifend.</p> -->
                        <img src="assets/login/images/image.jpg" alt="" />
                    </div>
                </div>
                <div class="w3_info">
                    <h2>Login to your Account</h2>
                    <p>Enter your details to login.</p>
                    <form class="login-form" action="loginpro.php" method="post">
                        <label>USERNAME</label>
                        <div class="input-group">
                            <span class="fa fa-user" aria-hidden="true"></span>
                            <input type="text" name="name" placeholder="Enter User name" required="">
                        </div>
                        <label>Password</label>
                        <div class="input-group">
                            <span class="fa fa-lock" aria-hidden="true"></span>
                            <input type="Password" name="password" placeholder="Enter Password" required="">
                        </div>
                        <!-- <div class="login-check">
                            <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i> </i> Remember me</label>
                        </div> -->
                        <button class="btn btn-danger btn-block" type="submit">Login</button>
                    </form>
                    <!-- <p class="account">By clicking login, you agree to our <a href="#">Terms & Conditions!</a></p>
				<p class="account1">Dont have an account? <a href="#">Register here</a></p> -->
                </div>
            </div>
            <!-- //main content -->
        </div>
        <!-- footer -->
        <div class="footer">
            <p>&copy; Copyright ©<?php echo date("Y"); ?> All rights reserved | Designed & Developed by  <a href="https://www.skyydigital.com/" target="blank">SKY DIGITAL (PVT) LTD</a></p>
        </div>
        <!-- footer -->
    </div>

</body>

</html>