-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2020 at 10:54 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wefix_soft`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill_tbl`
--

CREATE TABLE `bill_tbl` (
  `bill_id` int(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `tell_num` varchar(100) NOT NULL,
  `discription` varchar(255) NOT NULL,
  `bill_logo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_tbl`
--

INSERT INTO `bill_tbl` (`bill_id`, `address`, `tell_num`, `discription`, `bill_logo`) VALUES
(1, 'No.**, ******, ****', '07* - ******* / 07* - *******', '<p style=\"text-align:center\">sdfg fgf gfg fdg fdg</p>\r\n\r\n<p style=\"text-align:center\"><strong>** Thank You **</strong></p>\r\n', 'product1.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `brand_tbl`
--

CREATE TABLE `brand_tbl` (
  `brand_id` int(25) NOT NULL,
  `brand_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand_tbl`
--

INSERT INTO `brand_tbl` (`brand_id`, `brand_name`) VALUES
(5, 'Brand'),
(6, 'Test brand 2');

-- --------------------------------------------------------

--
-- Table structure for table `category_tbl`
--

CREATE TABLE `category_tbl` (
  `cate_id` int(25) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cate_code` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_tbl`
--

INSERT INTO `category_tbl` (`cate_id`, `cat_name`, `cate_code`) VALUES
(1, 'aaaaaaaaa', ''),
(2, 'cat 2 test', '2');

-- --------------------------------------------------------

--
-- Table structure for table `customer_tbl`
--

CREATE TABLE `customer_tbl` (
  `cus_id` int(25) NOT NULL,
  `cus_name` varchar(50) NOT NULL,
  `cus_phone` varchar(20) NOT NULL,
  `cus_email` varchar(50) NOT NULL,
  `cus_custom_feild` varchar(100) NOT NULL,
  `added_user` int(100) NOT NULL,
  `added_date` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `credit` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_tbl`
--

INSERT INTO `customer_tbl` (`cus_id`, `cus_name`, `cus_phone`, `cus_email`, `cus_custom_feild`, `added_user`, `added_date`, `address`, `credit`, `company`) VALUES
(1, 'kasun', '0718022765', '-@gmail.com', 'asasasa sas sas', 1, '11/13/2019', 'galle', '', ''),
(2, 'kasun', '0766581526', '-@gmail.com', '  vvvvvvvvvvv', 1, '11/28/2019', 'galle', '3 vbnnbvnb', 'aaaaaa');

-- --------------------------------------------------------

--
-- Table structure for table `email_table`
--

CREATE TABLE `email_table` (
  `mail_id` int(100) NOT NULL,
  `mail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_table`
--

INSERT INTO `email_table` (`mail_id`, `mail`) VALUES
(3, 'skydigitalweb01@gmail.com'),
(4, 'skydigitalweb06@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `grndetail_tbl`
--

CREATE TABLE `grndetail_tbl` (
  `detail_id` int(50) NOT NULL,
  `grnPro_id` int(50) NOT NULL,
  `unit` int(100) NOT NULL,
  `qty` int(100) NOT NULL,
  `cost` double NOT NULL,
  `price` double NOT NULL,
  `exDate` varchar(20) NOT NULL,
  `discount` double NOT NULL,
  `freeQty` int(100) NOT NULL,
  `grn_num` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grndetail_tbl`
--

INSERT INTO `grndetail_tbl` (`detail_id`, `grnPro_id`, `unit`, `qty`, `cost`, `price`, `exDate`, `discount`, `freeQty`, `grn_num`) VALUES
(1, 1, 0, 150, 10, 15, '2020-01-14', 0, 0, 1),
(2, 3, 0, 100, 10, 21, '2020-01-14', 0, 5, 1),
(3, 2, 0, 50, 20, 25, '2020-01-15', 0, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `grn_tbl`
--

CREATE TABLE `grn_tbl` (
  `grn_id` int(50) NOT NULL,
  `grn_num` varchar(100) NOT NULL,
  `grn_received` varchar(100) NOT NULL,
  `grn_suppid` int(100) NOT NULL,
  `grn_locid` int(100) NOT NULL,
  `grn_com_no` varchar(100) NOT NULL,
  `grn_due_on` varchar(20) NOT NULL,
  `grn_value` double NOT NULL,
  `grn_disc` double NOT NULL,
  `net_value` double NOT NULL,
  `invoice_value` double NOT NULL,
  `added_user` int(100) NOT NULL,
  `added_date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grn_tbl`
--

INSERT INTO `grn_tbl` (`grn_id`, `grn_num`, `grn_received`, `grn_suppid`, `grn_locid`, `grn_com_no`, `grn_due_on`, `grn_value`, `grn_disc`, `net_value`, `invoice_value`, `added_user`, `added_date`) VALUES
(1, '3500', '2020-01-15', 5, 2, '7686', '2020-01-15', 3500, 0, 3500, 3500, 1, '01/14/2020');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_additional_tbl`
--

CREATE TABLE `invoice_additional_tbl` (
  `additional_id` int(255) NOT NULL,
  `mainDec` text NOT NULL,
  `subDesc` text NOT NULL,
  `additional_price` double NOT NULL,
  `invoice_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details_tbl`
--

CREATE TABLE `invoice_details_tbl` (
  `invoice_det_id` int(255) NOT NULL,
  `invoice_id` int(255) NOT NULL,
  `stock_id` int(255) NOT NULL,
  `pro_code` varchar(255) NOT NULL,
  `pro_name` varchar(255) NOT NULL,
  `totQty` int(255) NOT NULL,
  `invoice_desc` text NOT NULL,
  `invoice_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_details_tbl`
--

INSERT INTO `invoice_details_tbl` (`invoice_det_id`, `invoice_id`, `stock_id`, `pro_code`, `pro_name`, `totQty`, `invoice_desc`, `invoice_price`) VALUES
(3, 2, 1, '1', 'aaaaaaaaa', 1, 'aaaaaaaaa', 15),
(4, 2, 2, '2', 'cccccccccc', 1, 'bbbbbbbbbb', 21),
(5, 3, 1, '1', 'aaaaaaaaa', 10, '-', 15),
(6, 3, 2, '2', 'cccccccccc', 10, '-', 21);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_lapack_tbl`
--

CREATE TABLE `invoice_lapack_tbl` (
  `invLab_id` int(255) NOT NULL,
  `invPack_id` int(255) NOT NULL,
  `invoiceRef_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_lapack_tbl`
--

INSERT INTO `invoice_lapack_tbl` (`invLab_id`, `invPack_id`, `invoiceRef_id`) VALUES
(2, 5, 2),
(3, 5, 3),
(4, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_tbl`
--

CREATE TABLE `invoice_tbl` (
  `invoice_id` int(255) NOT NULL,
  `invoice_job` int(255) NOT NULL,
  `added_user` int(255) NOT NULL,
  `added_date` varchar(255) NOT NULL,
  `added_time` varchar(255) NOT NULL,
  `revise_invoice` int(100) NOT NULL,
  `reInvoice` int(100) NOT NULL,
  `invoice_no` varchar(255) NOT NULL,
  `quoteRef_id` int(255) NOT NULL,
  `invCus_id` int(255) NOT NULL,
  `inv_disc` double NOT NULL,
  `payment` int(100) NOT NULL,
  `add_tax` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_tbl`
--

INSERT INTO `invoice_tbl` (`invoice_id`, `invoice_job`, `added_user`, `added_date`, `added_time`, `revise_invoice`, `reInvoice`, `invoice_no`, `quoteRef_id`, `invCus_id`, `inv_disc`, `payment`, `add_tax`) VALUES
(2, 4, 1, '01/20/2020', '01:56:07pm', 0, 0, 'WEFIX/IN/2020/P/1', 1, 1, 0, 0, 0),
(3, 1, 1, '01/20/2020', '02:23:20pm', 0, 0, 'WEFIX/IN/2020//3', 1, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `invo_quotesetting_tbl`
--

CREATE TABLE `invo_quotesetting_tbl` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `footer` text NOT NULL,
  `inFooter` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invo_quotesetting_tbl`
--

INSERT INTO `invo_quotesetting_tbl` (`id`, `name`, `number`, `email`, `image`, `address`, `footer`, `inFooter`) VALUES
(1, 'APS Lanka (Pvt.) Ltd.', 'Tel: +94 (0)112 548808, Fax: +94 (0)112 548724', 'E-mail: info@apslanka.com, Web: www.apslanka.com', 'logo1.jpg', '119/1, New Kandy Road, Kothalawela, Kaduwela, Sri Lanka', '<p>Terms &amp; Conditions<br />1 . 50% with official purchase order<br />2 . Quoted with Material.<br />3 . Our quotation is valid for a period of 15 days from date of quotation</p>\r\n<p>We do trust that our proposal in line with your requirement and if you need further clarification regarding the above,<br />please do not hesitate to contact or communicate to us.</p>\r\n<p>Thank You, Yours Faithfully,<br />APS Lanka (Pvt.) Ltd.</p>\r\n<p>KUMARA RANASINGHE* (TECHNICAL OFFICER)<br />On Behalf of APS Lanka (Pvt.) Ltd</p>\r\n<p>&nbsp;</p>\r\n<p><br />*Electronically submitted signature not required</p>', '<p>We Thank you for your Business with us.........<br /><strong>WEFIX</strong><br />by<br />APS Lanka (pvt.) Ltd.</p>\r\n<p>APS Lanka Authorized by :</p>\r\n<p>APS VAT REGISTRATION NO: 114800503-7000<br />APS NBT REGISTRATION NO: 114800503-9000</p>\r\n<p>Note: Please Draw the Cheques in Favour of \"APS Lanka (pvt) Ltd\"<br /><strong>Bank Details:</strong><br />Bank-Sampath Bank,Battaramulla<br />Account Number: 1061 1401 2336<br />Account Name: APS Lanka (pvt) Ltd</p>\r\n<p style=\"text-align: center;\"><img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gA8Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2OTApLCBxdWFsaXR5ID0gMTAwCv/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIADwAjgMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP7+KKi3x/34v/Hf/iqloAKKi3x/34v/AB3/AOKqWgCIjHOOOPXknt29+gHpxxQMAZJHGf1GMZ/DqOPzpC6/L8+cd+Bj3wQc/wD1q+OP2rfj0/w40aPwj4Yn/wCKu8QW8knnxEMdG0o/K94SDgXl2d0emKcb5VZzkKM/CeIfHvD/AIZ8J5xxhxJiPYZdllG7SX+0YrEv/dMHhVb3pzk9FqlrJ7a+zw9kOY8TZvgsjyqk6+MxtZQiukV9qc30jGKu+9klq1bT+N/7VPhv4ZNL4d0GO38UeMQv76zSYLp2kAgEPq13giFT1CqNxxyQeK/Ofxn+0D8WvG8jyap4v1LTrVRgaZ4euDo+n456valmY+pZiSepzyfHJJJJJJJJJPOmm+hzkH8/zH40yv8AErxc+lB4qeLGY4y2cZlw1wu63+w8O5HiMZhcO8Nt/tuPwSX9o/PTfRLQ/u3grwd4X4TweHcsroZ1mrs8Rm2Pw31lXVtcDHVRSe2l1peTepck1C9uJPtFxeXk0vH7+e4579B/X+ddl4c+KPxG8IXEdx4d8ZeItOEXHkG/vLrTT/243n/Er6D29OlcDUfmRx58yT6f55/z6d/wPJ844ky/MKeM4fzTOMFmn8f2+VYjGYbE/wDlmfoWY5dleNw/1fMcHllfCf8APjH4bBvDfr/XU/SL4R/tqi5u7PQvipbQ2zTOYIvFVjCyW4cD5f7UsVDG2JIwWiGBkExk5r9C7W7tdStIbuynhubW5iWaGeIiWGaGQBldWUkEFSOMngkeor+dLdF/z1h/z+Nfbn7Jfx8ufCut2vw48S3bT+G9auzFoE8pw2i6lOGK2IPQ2N8x3REnCScggMwb/Sj6Mn0tOLHnGXcB+Ln1zG4bMa1DBZRxjjsOsLicPi7O2Czm9lq7Lmesb3va5/Lnir4N5VRwWJ4i4M9lRnh4yxGPyGhX57Ybd43BJvmstW4JtO3upSST/WCiiiv9RD+WgooooAKKKKAP8d3/AIKufs6P+yX/AMFIf2u/grZWcujaN4Z+NGveJfBNurCQWfgPx+P+FkfDyPeoAYx+GvGWhxscA7lIKg9P9Vv9n39qLQPHf7BPwl/bF8T3wbQNa/ZV8I/H7xjdxgBoIV+Fln468XNjJAa3aPUAqkD/AFYbG1hX8SH/AAeAfs5DwX+1v+zv+01plgsWmfHH4R6j4B8Q3HmKz3vjD4N6v5yytHgNGi+CvG2k2q7shjaOytkMq/QXwe/bhfRP+DSD4nWaat5vjPw3rnjr9h0OFHMfxM+JFhrk+nseP+PP4B/E+Zw3PFqq9OgB/F/8TPHHiX4sfET4ifFjxR515r3xI8eeKPHHiq+/5dv+El8bazqfiS+Pf+XFf6yX/BE/4/Wvxt/4JLfsZfE3WNVgaTwt8ELT4ceKdRnIiFrd/Ae81X4U6pc35Awkv2fwOt/dPyT5zSsSzNX+ej8NP2MrvXP+CE37UP7Yy6ZGZ9B/bu+AelJqYI81PA/gj4eeNfBersVByNN1Xx7+01pAViMM0LBCdrV+xP8AwSD/AG1pvhN/wbz/APBWLQZ9XEWu/Ah/Fek+EHwDcaNL+1n4S0r4Y/DpX7nyvHst5LF6NvYdaAP5zhZeJf8AgpV/wUwu4bF7qLVP2zf2xL+9EoCrNoOgfFf4p/a727CgALp3gzwrqGAowFAwBX98v7SHxl8F/DHQ/Ffj67jXTfAnhCzsPDvgjw5YDGPDeiWn/COeCvCOjjOTnTdPHGScuCeATX8t/wDwbDfs7P8AE39tz4m/tAXtgJtC/ZO/Z+8e+LtPvT/y7/Ej4g6Rqfw48L2Q9SPCl/4+f6L6V+ov/BUvxDqFvovwh8LxyZ0vUtS8X67ej/p50Sz06ysf/Uixx6cV/AH0y6VTivi/wf8AC/EYitRyXOMZnHEea+wtbE/2YrLTsro/tX6FHBGD4x46xlLFuzVbC4DbVYaXNjsfZvS8owttdat3923h9nrH7XX7cWuavP4c1Sbwh8O7O7+ziKHV7zw54L00DGbS8vrEf2r4vvshSTgLnHA4FbOs/sQftV/CO3k8X/Djx/FrGqadD9tnsfB+v+JNE8R3Oev2OyvP7P0q/wA9MEgE9Otfpt4Pt9I+A37M9hfaHpUOp2XgT4Wz+KDY285tv7b1HTPD3/CRXhN9k8a9qZ3HPH3iBzx8CD/gqpc9/gfD7Y+I5H/ukHP6V/H+T8WceZ5mGaYPw04L4V/1IyHMv7Kr5VXw+Tr6z/2GfXNkm/LyP9KMp4o44zjGZjgvDrhPhtcJZPjFlf8AZNfC5O/rK2X15413cpKOurs7Jt9faf2Mv2w9Y+KeqSfCP4sRw2XxL06G4/svVPs/9m/8JJ/Zv/H9Z3dj/wAw3XdP5yM4zyMd/h//AIK6f8lh+F3/AGTaf/1JdTr9ZPglZfDL4t+E/CXx8i+D/gPw14y8UjUNbF8NI0jUtatdR+2alZC7/wCEp/svTtTN/wBunA4GK/Aj9uX48a58cfixHb+KPh//AMK9174Yw6x4A1XSv+Eg/wCEk+03Om+JNT+3Xn27+zPDv+f1/qv9nfwnT4o+l5iOKOG+F6OT5Nwfw3xBQ4xyrEZhk+J/s3O8ywf1P/Y8D/0AH+dv06+JOG6XC2YZfl+VYzhrH5lnGX0P7Ku8Vhf7Sy3GL699Sx2EtZdl89L2PRPg9/wTX+Kfxk+HHhP4maH48+Hum6X4w024vrGy1X/hJP7Stj9s+x4vPsel192fsf8A7C/xo/Zn+Mln8QLzx54D1Lw5eaPrHhzxHpWh3HiX+07i2uf9Msfsf2zTP+YfqmnaD+HTFfJfwO/4KaXHwX+E/gv4Xp8F4fEh8H6bcaUNcPxBOm/2l/pmp3nFj/wg2o/5Ffoh+yJ+2p4g/ao8WeKNHT4PQeCdB8K6Pb6rqviP/hOP7b/4mWpXhs9D0f7F/wAIzp3T+VfqH0sOLPp4ZPkfjJ/rJkfh7g/BGjjOIMD9e+r8B/2l/qlic4+p5H/zM/7U+vn4J4T5X4B4zMOC/wCz8ZxJW439jl9f2H1jOPq39pfU/wDbj7S/4LW/sqftuf8ABSv/AIJ+fBTQv2OtVTXPEOheORq3xb+FC+MrH4bj4n6TbaNq2hXFrLqniDUdP8M3SeHtZSS7/wCEY8QMkdxJcMkbPJFtH1H/AMEDf2Pf2wf2H/2IZfhF+2br/wBo8c3nxP13xL4J8BDxja+O4vhV8P73w/4ZtbTwdF4rstRv9KvFTWbDX9Y+w6VJ9ksxeMY2lknnEP8AOR/wcR+OvGXhb9kX9jh/C3irxF4Udvj58ddPubnw3q93ojXdongT4a3qKzddql3KqTgFnx1bP3t/wakWz/Hb9gH9sbwP8YdQ1z4h+G/FHx51Dwjrdn4s1W/1Se88N6z8IfCFlqGmNJORJEk0LyoTAQ5YKTyq5/cPBDiTGcV+Efh/xDmCVbHZlwrk+Ix9e2rxNvqb67tq9+u2+p+a8Z5dDJuK+JMqw+uFwWdZhQoenM2l8k7fLzP7BKK/yt/+Con7MX7Zn/BGv9tXQr7wR8bfi5ffDOfxVF8S/wBlz4sXXjHxHfLfReHNVW8k8K+KA6LpeqeJ/AsUken+I9OeNUvLKe3u41aC5iZvvP8A4K5/8HEV1+2R+w1+zp8D/wBn+91v4b/EX4p6JZeM/wBsOTw/carpFx4N13wdqcdnZ/CzQdSMEb+R4s8U6e/jOF98p/sex0yF5GnW5eX9UPmT/RVor+UP/g3E/wCCWnxQ+B3gTT/24f2ute8c6h8afif4cV/g38L/ABj4n1zUIPhT8OPEypI3izX9J1J9v/CzfiHbOtxfzzIVgsHCwq9xcpNaf1eUAfzc/wDB03+zkfjT/wAEuPEXxG02yS51/wDZn+KPgT4tQncqynw5q97P8M/FsUKnBkcWPjmDUmC5ITTZHIIDMP8AO8s/2nvEdp+xJ4n/AGNcXZ8M6v8AtQ+Ef2klbA+zrrWj/Czxv8ONQUHGQGPiDw+SBjJVSRkAj/Yg/aP+Cfhz9pP9n742fs++LZWt/DXxq+Fnjz4Xaxex26Xc2nWfjfw1qXhyTVLa2kKpNd6WdQF/aozIDcW0YLqMsP417L/gzBtor20l1D/gozLfWcLxm6t4/wBkw2ct3FGCRG0y/tMShNxwN2xgoJO1jwQD9NP2ff2Ci3/Bsu37MMmkKfF/xO/Yx8cfHFNPG1prr4oeOV1P9o7wHb3LBiBNY+IJfCekzKOVWx2sA6MD/ngfDf8AaM8QfDf9m/8Aac/Z70vzf7G/aW1L4Hz+I88Wxtvgn4k8TeMLHn/sKeIv/wBVf7Q2k6LpWg6Pp3h/SrG1sdF0nT7PSNN06CIC3tdMsbaO0s7NEJI8uGJFjQHPygA5Ylj/ABL+K/8AgzR03WvFHiPWfD3/AAUEHhbQtT8Ralquh+GV/ZSi1eLQNJvLsXlvokepT/tD2ks8VvJlGm+zRF1AYRjOxQD7M/4NVP2VV8Af8E1fiH8Zda05bfxB+1r8SfFdxZ3gkVjcfDb4YLqfw68MW7IMmJ7Xxr/wtOc7jl0vYmCgAFvGv+ChPwH1v4jfDCS50fTJbvxv8KNZ1DVP7Kgg+0XV1p4AsvFNlZg9yBwf9kdyK/qM/ZS/Z/8ADn7Kn7NPwJ/Zs8J3bX+hfBP4W+Dfhxb6s9klhNr1z4Y0Oz0/U/EN3aRv5dnfeJdTjvNbuYVZ1juL2YLI64c/CH7XnwqufB3j2bxlp1szaB4zlE0kqEH7JrmB9stcdQNSADAY+UkgdAT/AAp9Nfh/PMuy/gjxc4bwyxmK8PcyxFDNaCeryPMkru3aMklfbXzP6l+ih4iVeCOPv3dWKq4v6rjcAq9msRi8tcn9Ueqd6lNySX93Z2Z+Af7HX7aXgO78C6D8NPil4gsvCvifwvptvoml63rlx9l0TxHo1sPsVibu+wP7P1CxxznBBBPIY4+vfHH7TPwD8AaHcaxrHxH8IXghhM8OleHNXsvEmt6l0yLSx0jk59gc9K8o+M/7B3wX+LeqXfiSzj1L4feKNSlE99f+Ffsf9nalcD/l8vdEvOufXrxznrXifhv/AIJd+ALO/juPFHxL8U67Ywy8WOlaPZ+HRcHt/wBDASB1IBHfnoa/zzxlXwL4hzCpxRiOIOKuG6uMrLG5rwrQy94n/af+oPHavRfkf6W4ufgvneLqcQTzziXhqrjKzxGO4boZcsUvrN7tYLH6JXbdl5tq2qN79mv9qz44ftD/ABY1PT9L8H+D9C+EOgS3F7qmq3Gj6vca3p+nD/kB6Qb4eJf7K/t7UD1IHAbgGvij/grr/wAlg+GH/ZOLn/1I9Ur9yPAfw+8F/C/w5aeFPAfh+z8OaFZ8fYbEj/SLnP8Ax+Xl7ef8TO/v+MnrnjJOBj5k/aI/Yr8D/tMfETwv408eeLPEmm6X4a8N/wBh/wDCOeHPsVtc6l/xN9TvBeXmu3h1H64r9j+in9IHwv8ACP6TGX+JmcZfW4V8Psn4V4oyr2GVYfGZlmWZYnE5P9TwP1zT/f8AMPv/AAR/Jn0o8i/4ipwvmOR+H/D+Dyyk8Zk/1DD18Tf/AGXC4xfXsZjMc29e9na1ut2bn7C8cf8Awyf8E/3eB/witx7Y/wCKk1PjuPf1r60ri/h/4D8N/C/wX4f+H/g+zm03w54Vs/sOlQT3F5qVyLb/AK/rz/Pqea9u+G3gHV/iT4z0jwjpEc3majN/p85OP7N021IF7djJAyTgAdzx71/NfHmeZh4ueMHFmM4X/tjGYXjzjzOMdkeVV/8Aefq2d5x9cwJrkeAw3B3BWTwzn6nRqZBkGX0MfiOi/s3B3v8A1+eh+BP/AAczeHn0X9jP9ge4kj8mfxB8X/j7rrA9R5nhvwRZg++Qv6Y5r79/4M6gD+xT+1LnoP2pF/L/AIVR4DP9K/TH/grv/wAEaLL/AIKl/DP9nf4aaT8fY/2eNP8AgFrPifVrK7Hwnj+KI1uDxDoOk6Lb6e2nP4++HaaculrpUcqSJcTNL5zqY4fKCvL/AMEw/wDgkj8Q/wDgl3+yz+0H8Bfhf+1Rpfjj4h/GPxXqPjfwf8Xda+A9vpOlfDfxDJ4K03wvpU978PB8VtXt/FsWnSabFeRq+u6YlwUWK+hmgBiP+9/hxwquCOBODuEotN5DkGXZXW1vfEYXCKLa8r3X3H+ePEWcVc8zvOM4nvmeZ4nHNdlKTsvXlsvW9j8Qf+DsX/go58INT8I6J/wTl8FaF4U+IXxOsfEvhf4mfFnxjc28Oq3PwPFjFLf+E/DXh+aMMbH4l+NI5F+yReZm3snjSZBOzRwfyB6n8Lvj5+w18Rf2XvjH8UfhFYafN4l0T4XftS/CLQfiXoNl4m8FfETwIfEK6j4UTxPoQJ+26J4hj0GOTVdIbB2PG+CrKT/al4F/4NIdLk/aL0L45/tK/t46x+0ppE/xKT4m/FvwhrP7P8/hjVPjLqD62dd16y8TeNr348eMr2xh8S3GEvpILOQwqSyLKQQP26/4Kz/8EkvhR/wVP+AfhP4Wat4jsvgz8Q/hnr9vrXwp+Lml+CrTxbdeCbOeBNP8SeF4/DDa14WtdR8N+JdJSKxu7CTUII7cW8MtsqAyxy/cnkne/sw/8FUP2av2lP2E9D/bq0nVrvT/AAokGkeHfHPgDSrefxP4+8M/GjU9Q0XwzbfBPTfDukJLqPiXx34k8aeIvD/hzwBpNlAtx4vl8TeGbi0jh/tRVh6z4Af8FFfhJ8ddfu/C+r+FPGfwS1WTw9e+LPC2o+P/ABX8CPGHgnx9oGj6jo2m+Ih4R+JPwG+L/wAafh3P4i8ITeLPBj+MfBmreI9H8XaJD4w8O3R0q7sbma7g/FP9i/8A4Nx/jf8AsRJ8QtG8A/8ABSV9b8IfEeX4f+LLvwjdfssrp+nad8Yvgd470j4s/AH4r2s1z+0FritqXw2+Ivh7RE1XQ3RYfHXw4fXvBd5dWEV2LyL9Kv2ff+CX954fuNFtv2hNU+E2q+APBHhjW9G8C/Bz4K+HPGuheCYfFHivTvhJpPin4l6/q/j3xT4n8TXniC9sPg7odro+hWdxb6HoMGveJdr6hdXou6AP2IooooAKKKKAIgdpPcgDPfAzg9/oPqD2rkvG3gzQ/iB4c1Lwx4itVudN1OB4ZRnDozAhZEI+66HBQnk4xyOnWHoD3IIPvj+v9eetOBwpIAGD+fTr37+vpXDmGW4LOcvxuWZlhKOMy/H0a+Bx2DrLmoYjD1k41ITi1qpJ67b9Gkx0cTVweIp16FapQxNCqq9GtSfLKEou6aatZpr7tNU2n+IHxm+AXjD4PalI1zDJrHhOaUNpfiGCAG3AIO201g8/YbxsHZIuUkAyhNeFV/RNfWdpf2slrfW0F3bTo8U0FxGssUsZXJV0YEMPr0wMYIBr4y+Lf7LvwlngudX0vTNQ8MXSbpDF4dvY7WwlkBA3y6beWt/Ysf8Adt1Htiv8nfH/AOhPT4ZjmHGfAOeYOlw/VftpcPZ19cnXwDtf/YMdBzaVtuZJ9N9T+suA/H2WJjhMo4pwFatjotYejm+A5bYi7UV9cwTlCErt6uErbuyvY/Kaiu+1nwnptjqItoJb3ymlGVkmifqueD5A/XNfYHwi/Zt+HPiRoNQ15vEOpIwIfTn1ZLWwfgcsun2dndk/9vePbrX8W8E+Ged8dcSQ4ay3FZdhcXVrexdbGV8bLD22vyx5pd+h+8cScZZZwvlUc2x1DGVqMtqGHcdXa/WUUtPz+Z8YeCPAviv4ha5BoPhXSJdRvpf+Pgk/6Nplt/z93l97k/nkV+w3wG+BOj/BvQnj8xdR8S6tGkmu6wV2mSTaP9Ftty7o7OMg7EOCc7iAQMep+E/B3hfwZpiab4V0PTtCsUYhbXTrdYIxkcnAyxJ9yeOK67GVA9hX+wH0d/olcNeDVSnxNnOLo8UcbSpKNHHPDRoZfkqd1KGUUn78Lq6c5paN8qW5/GXiR4v5px1BYDCUP7I4eTTWChLmr4qz0ljJxfLKzTtBNrZyvok6iiiv7CPyEKKKKACiiigD/9k=\" width=\"106\" height=\"45\" /></p>\r\n<p style=\"text-align: center;\"><strong>WEFIX (PVT) LTD</strong><br />119/1, New Kandy Road, Kothalawala, Kaduwela, Sri Lanka<br />+94 112 548808 | +94 112 548724, <a href=\"mailto:info@apslanka.com,\" target=\"_blank\" rel=\"noopener\">info@apslanka.com,</a> <a href=\"http://www.apslanka.com\" target=\"_blank\" rel=\"noopener\">www.apslanka.com</a></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `jobcategory_tbl`
--

CREATE TABLE `jobcategory_tbl` (
  `jobCat_id` int(255) NOT NULL,
  `jobCat_name` varchar(100) NOT NULL,
  `jobCat_subName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobcategory_tbl`
--

INSERT INTO `jobcategory_tbl` (`jobCat_id`, `jobCat_name`, `jobCat_subName`) VALUES
(3, 'Electric', 'E'),
(4, 'Plumbing', 'P');

-- --------------------------------------------------------

--
-- Table structure for table `jobfile_tbl`
--

CREATE TABLE `jobfile_tbl` (
  `file_id` int(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `added_date` varchar(255) NOT NULL,
  `added_user` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobfile_tbl`
--

INSERT INTO `jobfile_tbl` (`file_id`, `file_name`, `added_date`, `added_user`) VALUES
(1, 'Daily Job.xml', '11/15/2019', 1),
(2, 'Daily Job Report from 2019-11-01 to 2019-11-01.csv', '11/15/2019', 1),
(3, 'Daily Job Report from 2019-11-01 to 2019-11-01.csv', '11/15/2019', 1),
(4, 'Daily Job Report from 2019-11-01 to 2019-11-01.csv', '11/15/2019', 1),
(5, 'Daily Job Report from 2019-11-01 to 2019-11-01.csv', '11/19/2019', 1),
(6, 'Daily Job Report from 2019-11-01 to 2019-11-01.csv', '11/19/2019', 1),
(7, 'Daily Job Report from 2019-11-01 to 2019-11-01.csv', '11/19/2019', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_tbl`
--

CREATE TABLE `job_tbl` (
  `job_id` int(255) NOT NULL,
  `job_no` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_category` varchar(255) NOT NULL,
  `service_type` varchar(255) NOT NULL,
  `job_type` varchar(255) NOT NULL,
  `job_status` varchar(255) NOT NULL,
  `create_on` varchar(255) NOT NULL,
  `assign_on` varchar(255) NOT NULL,
  `dispatch_on` varchar(255) NOT NULL,
  `complete_on` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `job_file` int(255) NOT NULL,
  `send_quote` int(100) NOT NULL,
  `send_invoice` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_tbl`
--

INSERT INTO `job_tbl` (`job_id`, `job_no`, `customer_name`, `customer_category`, `service_type`, `job_type`, `job_status`, `create_on`, `assign_on`, `dispatch_on`, `complete_on`, `date`, `job_file`, `send_quote`, `send_invoice`) VALUES
(1, 'WFNM1130', 'Gflock Mr.Chathura', 'Non Member', 'Electric', 'New', 'Send Invoice', '01-Nov-2019 09:01 AM', '01-Nov-2019 09:03 AM', '01-Nov-2019 09:03 AM', '01-Nov-2019 04:12 PM', '01-Nov-2019', 7, 0, 1),
(2, 'WFNM1131', 'Mr. EBENZER', 'Non Member', 'Electric', 'New', 'Send Invoice', '01-Nov-2019 09:04 AM', '01-Nov-2019 09:04 AM', '01-Nov-2019 09:05 AM', '01-Nov-2019 04:12 PM', '01-Nov-2019', 7, 0, 1),
(3, 'WFNM1132', 'Mr. EBENZER', 'Non Member', 'Electric', 'New', 'Send Invoice', '01-Nov-2019 09:07 AM', '01-Nov-2019 09:08 AM', '01-Nov-2019 09:10 AM', '01-Nov-2019 04:12 PM', '01-Nov-2019', 7, 0, 1),
(4, 'WFNM1133', 'The Manger _ Athurugiriya Cargills Food Company pvt Ltd', 'Non Member', 'Plumbing', 'New', 'Send Invoice', '01-Nov-2019 10:02 AM', '01-Nov-2019 04:12 PM', '01-Nov-2019 04:12 PM', '01-Nov-2019 04:12 PM', '01-Nov-2019', 7, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `labourpack_tbl`
--

CREATE TABLE `labourpack_tbl` (
  `labourPack_id` int(255) NOT NULL,
  `labourPack_name` varchar(255) NOT NULL,
  `labourPack_price` double NOT NULL,
  `added_user` int(255) NOT NULL,
  `added_date` varchar(255) NOT NULL,
  `labourPack_cost` double NOT NULL,
  `labourPack_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `labourpack_tbl`
--

INSERT INTO `labourpack_tbl` (`labourPack_id`, `labourPack_name`, `labourPack_price`, `added_user`, `added_date`, `labourPack_cost`, `labourPack_type`) VALUES
(4, 'fdgdfgfg', 2000, 1, '11/19/2019', 1200, 'gfdg'),
(5, 'fsf sf sf', 3500, 1, '11/28/2019', 3200, 'gdfgfdg'),
(6, 'aaaaaaaaa', 3000, 1, '11/28/2019', 2750, 'ttttttttttttttttt');

-- --------------------------------------------------------

--
-- Table structure for table `labour_tbl`
--

CREATE TABLE `labour_tbl` (
  `labour_id` int(255) NOT NULL,
  `labour_name` varchar(255) NOT NULL,
  `labour_email` varchar(255) NOT NULL,
  `labour_phone` varchar(255) NOT NULL,
  `labour_roll` varchar(255) NOT NULL,
  `labour_field` text NOT NULL,
  `labour_address` varchar(255) NOT NULL,
  `added_date` varchar(100) NOT NULL,
  `added_user` int(255) NOT NULL,
  `nic` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `labour_tbl`
--

INSERT INTO `labour_tbl` (`labour_id`, `labour_name`, `labour_email`, `labour_phone`, `labour_roll`, `labour_field`, `labour_address`, `added_date`, `added_user`, `nic`) VALUES
(2, 'kasun', 'kasun@gmail.com', '0766581526', 'asdsad sad sad', 'sadsad asd asdsad sadsadsa sadsa', 'galle', '11/14/2019', 1, '9716434390'),
(3, 'kasun', '-@gmail.com', '0766581526', 'dsadds', 'dasdsad', 'galle', '12/13/2019', 1, '4545454545'),
(4, 'viduranga', '-@gmail.com', '1111111111', 'aaaa', 'sdf', 'galle', '12/13/2019', 1, '4545454545');

-- --------------------------------------------------------

--
-- Table structure for table `location_tbl`
--

CREATE TABLE `location_tbl` (
  `loc_id` int(50) NOT NULL,
  `loc_name` varchar(100) NOT NULL,
  `loc_code` varchar(100) NOT NULL,
  `added_user` int(100) NOT NULL,
  `added_date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location_tbl`
--

INSERT INTO `location_tbl` (`loc_id`, `loc_name`, `loc_code`, `added_user`, `added_date`) VALUES
(1, 'galle', '1', 1, '09/04/2019'),
(2, 'galle', '2', 1, '09/04/2019');

-- --------------------------------------------------------

--
-- Table structure for table `payment_tbl`
--

CREATE TABLE `payment_tbl` (
  `pay_id` int(100) NOT NULL,
  `pay_inv` int(100) NOT NULL,
  `pay_price` double NOT NULL,
  `pay_date` varchar(255) NOT NULL,
  `pay_user` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_tbl`
--

INSERT INTO `payment_tbl` (`pay_id`, `pay_inv`, `pay_price`, `pay_date`, `pay_user`) VALUES
(1, 3, 500, '01/20/2020', 1),
(2, 3, 500, '01/20/2020', 1),
(3, 3, 760, '01/20/2020', 1),
(4, 3, 500, '01/20/2020', 1),
(5, 3, 500, '01/20/2020', 1),
(6, 3, 500, '01/20/2020', 1),
(7, 3, 500, '01/20/2020', 1),
(8, 3, 500, '01/20/2020', 1),
(9, 3, 500, '01/22/2020', 1),
(10, 3, 500, '01/22/2020', 1),
(11, 3, 100, '01/22/2020', 1),
(12, 2, 3436, '01/22/2020', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_tbl`
--

CREATE TABLE `products_tbl` (
  `pro_id` int(25) NOT NULL,
  `pro_name` varchar(100) NOT NULL,
  `pro_code` varchar(100) NOT NULL,
  `cat_id` int(25) NOT NULL,
  `subcat_id` int(25) NOT NULL,
  `brand_id` int(25) NOT NULL,
  `pro_desc` varchar(100) NOT NULL,
  `pro_cost` double NOT NULL,
  `pro_price` double NOT NULL,
  `pro_all_qty` double NOT NULL,
  `pro_img` varchar(100) NOT NULL,
  `rack_no` varchar(100) NOT NULL,
  `supplier_id` int(100) NOT NULL,
  `whole_sale_price` double NOT NULL,
  `added_date` varchar(100) NOT NULL,
  `added_user` int(50) NOT NULL,
  `pro_unit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_tbl`
--

INSERT INTO `products_tbl` (`pro_id`, `pro_name`, `pro_code`, `cat_id`, `subcat_id`, `brand_id`, `pro_desc`, `pro_cost`, `pro_price`, `pro_all_qty`, `pro_img`, `rack_no`, `supplier_id`, `whole_sale_price`, `added_date`, `added_user`, `pro_unit`) VALUES
(1, 'aaaaaaaaa', '11', 1, 11, 6, '-', 10, 15, 5, 'noImage.jpg', '7', 2, 12, '01/14/2020', 1, 'qty'),
(2, 'bbbbbbb', '22', 1, 11, 6, '-', 20, 25, 3, 'noImage.jpg', '3', 6, 22, '01/14/2020', 1, 'qty'),
(3, 'cccccccccc', '33', 1, 11, 6, '-', 10, 21, 10, 'noImage.jpg', '1', 5, 22, '01/14/2020', 1, 'qty');

-- --------------------------------------------------------

--
-- Table structure for table `pro_releas_tbl`
--

CREATE TABLE `pro_releas_tbl` (
  `releas_id` int(100) NOT NULL,
  `releas_quot_id` int(100) NOT NULL,
  `release_quot_det_id` int(100) NOT NULL,
  `releas_pro_name` varchar(255) NOT NULL,
  `pro_releas_qty` double NOT NULL,
  `releas_date` varchar(255) NOT NULL,
  `releas_user` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pro_releas_tbl`
--

INSERT INTO `pro_releas_tbl` (`releas_id`, `releas_quot_id`, `release_quot_det_id`, `releas_pro_name`, `pro_releas_qty`, `releas_date`, `releas_user`) VALUES
(1, 1, 1, 'aaaaaaaaa', 5, '01/18/2020', 1),
(2, 1, 2, 'cccccccccc', 5, '01/18/2020', 1),
(3, 1, 1, 'aaaaaaaaa', 2, '01/18/2020', 1),
(4, 1, 2, 'cccccccccc', 5, '01/18/2020', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quatation_tbl`
--

CREATE TABLE `quatation_tbl` (
  `quate_id` int(255) NOT NULL,
  `added_user` int(255) NOT NULL,
  `added_date` varchar(255) NOT NULL,
  `added_time` varchar(255) NOT NULL,
  `revise_quote` int(100) NOT NULL,
  `reQuote` int(11) NOT NULL,
  `quote_no` varchar(255) NOT NULL,
  `accept_qte` int(100) NOT NULL,
  `released_pro` int(100) NOT NULL,
  `quote_cusID` int(100) NOT NULL,
  `quote_disc` double NOT NULL,
  `quote_advance` double NOT NULL,
  `add_tax` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quatation_tbl`
--

INSERT INTO `quatation_tbl` (`quate_id`, `added_user`, `added_date`, `added_time`, `revise_quote`, `reQuote`, `quote_no`, `accept_qte`, `released_pro`, `quote_cusID`, `quote_disc`, `quote_advance`, `add_tax`) VALUES
(1, 1, '01/17/2020', '03:49:28pm', 0, 0, 'WEFIX/QT/2020/1', 1, 0, 1, 0, 100, 1),
(2, 1, '01/18/2020', '12:05:21pm', 0, 0, 'WEFIX/QT/2020/2', 0, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `quate_details_tbl`
--

CREATE TABLE `quate_details_tbl` (
  `quate_det_id` int(255) NOT NULL,
  `quate_id` int(255) NOT NULL,
  `stock_id` int(255) NOT NULL,
  `pro_code` varchar(255) NOT NULL,
  `pro_name` varchar(255) NOT NULL,
  `totQty` double NOT NULL,
  `quate_desc` text NOT NULL,
  `quote_price` double NOT NULL,
  `releas_qty` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quate_details_tbl`
--

INSERT INTO `quate_details_tbl` (`quate_det_id`, `quate_id`, `stock_id`, `pro_code`, `pro_name`, `totQty`, `quate_desc`, `quote_price`, `releas_qty`) VALUES
(1, 1, 1, '1', 'aaaaaaaaa', 10, '-', 15, 5),
(2, 1, 2, '2', 'cccccccccc', 10, '-', 21, 0),
(3, 2, 1, '1', 'aaaaaaaaa', 1, '-', 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quate_lapack_tbl`
--

CREATE TABLE `quate_lapack_tbl` (
  `qutLab_id` int(255) NOT NULL,
  `quitPack_id` int(255) NOT NULL,
  `quateRef_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quate_lapack_tbl`
--

INSERT INTO `quate_lapack_tbl` (`qutLab_id`, `quitPack_id`, `quateRef_id`) VALUES
(1, 5, 1),
(3, 5, 3),
(4, 5, 4),
(5, 5, 5),
(6, 5, 6),
(7, 5, 7),
(8, 5, 8),
(9, 5, 9),
(10, 4, 1),
(11, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `quote_additional_tbl`
--

CREATE TABLE `quote_additional_tbl` (
  `additional_id` int(255) NOT NULL,
  `mainDec` text NOT NULL,
  `subDesc` text NOT NULL,
  `additional_price` double NOT NULL,
  `quote_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `returnmaterial_tbl`
--

CREATE TABLE `returnmaterial_tbl` (
  `id` int(255) NOT NULL,
  `re_stock_id` int(255) NOT NULL,
  `re_invoice_id` int(255) NOT NULL,
  `re_qty` int(255) NOT NULL,
  `re_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_tbl`
--

CREATE TABLE `stock_tbl` (
  `stock_id` int(100) NOT NULL,
  `pro_id` int(100) NOT NULL,
  `stock_qty` int(100) NOT NULL,
  `stock_proPrice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_tbl`
--

INSERT INTO `stock_tbl` (`stock_id`, `pro_id`, `stock_qty`, `stock_proPrice`) VALUES
(1, 1, 149, 15),
(2, 3, 104, 21),
(3, 2, 52, 25);

-- --------------------------------------------------------

--
-- Table structure for table `subcategory_tbl`
--

CREATE TABLE `subcategory_tbl` (
  `subcate_id` int(25) NOT NULL,
  `subcate_name` varchar(100) NOT NULL,
  `subcate_code` varchar(100) NOT NULL,
  `cate_id` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategory_tbl`
--

INSERT INTO `subcategory_tbl` (`subcate_id`, `subcate_name`, `subcate_code`, `cate_id`) VALUES
(7, 'dfgfdg', '1', 4),
(9, 'Subcategory', '8', 1),
(11, 'dfgfdg', '11', 1),
(12, '', '', 0),
(13, 'cat 2 test', '13', 2),
(14, '', '', 0),
(15, '', '', 0),
(16, '', '', 0),
(17, 'kasun', '', 0),
(18, 'kasun', '', 0),
(19, '', '', 0),
(20, '', '', 0),
(21, '', '', 0),
(22, 'aaaaaaaaaaaaaaaaaa', '22', 2);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_tbl`
--

CREATE TABLE `supplier_tbl` (
  `supplier_id` int(25) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_email` varchar(100) NOT NULL,
  `supplier_phone` varchar(20) NOT NULL,
  `custom_field` varchar(150) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `site_name` varchar(100) NOT NULL,
  `added_user` int(100) NOT NULL,
  `added_date` varchar(100) NOT NULL,
  `credit_period` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_tbl`
--

INSERT INTO `supplier_tbl` (`supplier_id`, `supplier_name`, `supplier_email`, `supplier_phone`, `custom_field`, `contact_name`, `contact_phone`, `site_name`, `added_user`, `added_date`, `credit_period`) VALUES
(2, 'kasun', '-@gmail.com', '0766581526', 'assd das', 'vid', '0123456789', 'https://www.w3schools.com', 1, '11/11/2019', '2 month'),
(5, 'kasun', '-@gmail.com', '0766581526', 'wtrwvrtv wrt', 'ishanga', '0123456789', 'https://www.w3schools.com', 1, '12/12/2019', '2 month'),
(6, 'asdsa', '-@gmail.com', '0766581526', 'dasdasd', 'dsad', 'dsadsa', 'www.doubleA.com', 1, '12/13/2019', 'asdsa'),
(7, '', '', '', '', '', '', '', 1, '01/22/2020', '');

-- --------------------------------------------------------

--
-- Table structure for table `tax_tbl`
--

CREATE TABLE `tax_tbl` (
  `id` int(100) NOT NULL,
  `tax` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tax_tbl`
--

INSERT INTO `tax_tbl` (`id`, `tax`) VALUES
(1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE `user_tbl` (
  `user_id` int(25) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_pwd` varchar(100) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `user1` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `suppliers` int(11) NOT NULL,
  `jobCat` int(11) NOT NULL,
  `job` int(11) NOT NULL,
  `labour` int(11) NOT NULL,
  `labourPack` int(11) NOT NULL,
  `grn` int(11) NOT NULL,
  `release1` int(11) NOT NULL,
  `maReturn` int(11) NOT NULL,
  `invSett` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `email` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `subCate` int(11) NOT NULL,
  `brand` int(11) NOT NULL,
  `products` int(11) NOT NULL,
  `quote` int(11) NOT NULL,
  `reQuote` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `reInvoice` int(11) NOT NULL,
  `stockRepo` int(11) NOT NULL,
  `reProReport` int(11) NOT NULL,
  `matReReport` int(11) NOT NULL,
  `proReport` int(11) NOT NULL,
  `quoteReport` int(11) NOT NULL,
  `invReport` int(11) NOT NULL,
  `payReport` int(11) NOT NULL,
  `added_user` int(100) NOT NULL,
  `added_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_tbl`
--

INSERT INTO `user_tbl` (`user_id`, `user_name`, `user_pwd`, `user_type`, `user1`, `customer`, `suppliers`, `jobCat`, `job`, `labour`, `labourPack`, `grn`, `release1`, `maReturn`, `invSett`, `tax`, `email`, `category`, `subCate`, `brand`, `products`, `quote`, `reQuote`, `invoice`, `reInvoice`, `stockRepo`, `reProReport`, `matReReport`, `proReport`, `quoteReport`, `invReport`, `payReport`, `added_user`, `added_date`) VALUES
(1, 'admin', '12345', 'super admin', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '01/22/2020');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill_tbl`
--
ALTER TABLE `bill_tbl`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `brand_tbl`
--
ALTER TABLE `brand_tbl`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `category_tbl`
--
ALTER TABLE `category_tbl`
  ADD PRIMARY KEY (`cate_id`);

--
-- Indexes for table `customer_tbl`
--
ALTER TABLE `customer_tbl`
  ADD PRIMARY KEY (`cus_id`);

--
-- Indexes for table `email_table`
--
ALTER TABLE `email_table`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `grndetail_tbl`
--
ALTER TABLE `grndetail_tbl`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `grn_tbl`
--
ALTER TABLE `grn_tbl`
  ADD PRIMARY KEY (`grn_id`);

--
-- Indexes for table `invoice_additional_tbl`
--
ALTER TABLE `invoice_additional_tbl`
  ADD PRIMARY KEY (`additional_id`);

--
-- Indexes for table `invoice_details_tbl`
--
ALTER TABLE `invoice_details_tbl`
  ADD PRIMARY KEY (`invoice_det_id`);

--
-- Indexes for table `invoice_lapack_tbl`
--
ALTER TABLE `invoice_lapack_tbl`
  ADD PRIMARY KEY (`invLab_id`);

--
-- Indexes for table `invoice_tbl`
--
ALTER TABLE `invoice_tbl`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `invo_quotesetting_tbl`
--
ALTER TABLE `invo_quotesetting_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobcategory_tbl`
--
ALTER TABLE `jobcategory_tbl`
  ADD PRIMARY KEY (`jobCat_id`);

--
-- Indexes for table `jobfile_tbl`
--
ALTER TABLE `jobfile_tbl`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `job_tbl`
--
ALTER TABLE `job_tbl`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `labourpack_tbl`
--
ALTER TABLE `labourpack_tbl`
  ADD PRIMARY KEY (`labourPack_id`);

--
-- Indexes for table `labour_tbl`
--
ALTER TABLE `labour_tbl`
  ADD PRIMARY KEY (`labour_id`);

--
-- Indexes for table `location_tbl`
--
ALTER TABLE `location_tbl`
  ADD PRIMARY KEY (`loc_id`);

--
-- Indexes for table `payment_tbl`
--
ALTER TABLE `payment_tbl`
  ADD PRIMARY KEY (`pay_id`);

--
-- Indexes for table `products_tbl`
--
ALTER TABLE `products_tbl`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `pro_releas_tbl`
--
ALTER TABLE `pro_releas_tbl`
  ADD PRIMARY KEY (`releas_id`);

--
-- Indexes for table `quatation_tbl`
--
ALTER TABLE `quatation_tbl`
  ADD PRIMARY KEY (`quate_id`);

--
-- Indexes for table `quate_details_tbl`
--
ALTER TABLE `quate_details_tbl`
  ADD PRIMARY KEY (`quate_det_id`);

--
-- Indexes for table `quate_lapack_tbl`
--
ALTER TABLE `quate_lapack_tbl`
  ADD PRIMARY KEY (`qutLab_id`);

--
-- Indexes for table `quote_additional_tbl`
--
ALTER TABLE `quote_additional_tbl`
  ADD PRIMARY KEY (`additional_id`);

--
-- Indexes for table `returnmaterial_tbl`
--
ALTER TABLE `returnmaterial_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_tbl`
--
ALTER TABLE `stock_tbl`
  ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `subcategory_tbl`
--
ALTER TABLE `subcategory_tbl`
  ADD PRIMARY KEY (`subcate_id`);

--
-- Indexes for table `supplier_tbl`
--
ALTER TABLE `supplier_tbl`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `tax_tbl`
--
ALTER TABLE `tax_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill_tbl`
--
ALTER TABLE `bill_tbl`
  MODIFY `bill_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brand_tbl`
--
ALTER TABLE `brand_tbl`
  MODIFY `brand_id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category_tbl`
--
ALTER TABLE `category_tbl`
  MODIFY `cate_id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer_tbl`
--
ALTER TABLE `customer_tbl`
  MODIFY `cus_id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `email_table`
--
ALTER TABLE `email_table`
  MODIFY `mail_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `grndetail_tbl`
--
ALTER TABLE `grndetail_tbl`
  MODIFY `detail_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `grn_tbl`
--
ALTER TABLE `grn_tbl`
  MODIFY `grn_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invoice_additional_tbl`
--
ALTER TABLE `invoice_additional_tbl`
  MODIFY `additional_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_details_tbl`
--
ALTER TABLE `invoice_details_tbl`
  MODIFY `invoice_det_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `invoice_lapack_tbl`
--
ALTER TABLE `invoice_lapack_tbl`
  MODIFY `invLab_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `invoice_tbl`
--
ALTER TABLE `invoice_tbl`
  MODIFY `invoice_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invo_quotesetting_tbl`
--
ALTER TABLE `invo_quotesetting_tbl`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jobcategory_tbl`
--
ALTER TABLE `jobcategory_tbl`
  MODIFY `jobCat_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jobfile_tbl`
--
ALTER TABLE `jobfile_tbl`
  MODIFY `file_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `job_tbl`
--
ALTER TABLE `job_tbl`
  MODIFY `job_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `labourpack_tbl`
--
ALTER TABLE `labourpack_tbl`
  MODIFY `labourPack_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `labour_tbl`
--
ALTER TABLE `labour_tbl`
  MODIFY `labour_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `location_tbl`
--
ALTER TABLE `location_tbl`
  MODIFY `loc_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment_tbl`
--
ALTER TABLE `payment_tbl`
  MODIFY `pay_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products_tbl`
--
ALTER TABLE `products_tbl`
  MODIFY `pro_id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pro_releas_tbl`
--
ALTER TABLE `pro_releas_tbl`
  MODIFY `releas_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `quatation_tbl`
--
ALTER TABLE `quatation_tbl`
  MODIFY `quate_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `quate_details_tbl`
--
ALTER TABLE `quate_details_tbl`
  MODIFY `quate_det_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `quate_lapack_tbl`
--
ALTER TABLE `quate_lapack_tbl`
  MODIFY `qutLab_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `quote_additional_tbl`
--
ALTER TABLE `quote_additional_tbl`
  MODIFY `additional_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `returnmaterial_tbl`
--
ALTER TABLE `returnmaterial_tbl`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_tbl`
--
ALTER TABLE `stock_tbl`
  MODIFY `stock_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `subcategory_tbl`
--
ALTER TABLE `subcategory_tbl`
  MODIFY `subcate_id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `supplier_tbl`
--
ALTER TABLE `supplier_tbl`
  MODIFY `supplier_id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tax_tbl`
--
ALTER TABLE `tax_tbl`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_tbl`
--
ALTER TABLE `user_tbl`
  MODIFY `user_id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
