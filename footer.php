<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
						<ul class="nav">
							<li class="nav-item">
								<a target="_new" class="nav-link" href="https://wefix.lk/">
									WEFIX
								</a>
							</li>
							<!-- <li class="nav-item">
								<a class="nav-link" href="#">
									Help
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									Licenses
								</a>
							</li> -->
						</ul>
					</nav>
					<div class="copyright ml-auto"> Copyright ©<?php echo date("Y"); ?> All rights reserved | Designed & Developed by <a target="_new" href="https://www.skyydigital.com"> SKY DIGITAL (PVT) LTD</a>
					</div>
				</div>
			</footer>