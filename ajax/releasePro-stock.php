<?php
include('../db/dbConnection.php');

$data = (array) $_POST['data'];
$id = $data['quote_id'];

session_start();
$date = date("m/d/Y");
$user = $_SESSION['user_id'];
$ifSuccess = true;

mysqli_autocommit($connection, false);

for ($y = 0; $y < count($data['localObj']); $y++) {

    $detail_id = $data['localObj'][$y]['id'];
    $releaseQty = $data['localObj'][$y]['qty'];
    $name = $data['localObj'][$y]['name'];

    $query9 = "UPDATE quate_details_tbl SET releas_qty = releas_qty - $releaseQty WHERE quate_det_id='$detail_id'";

    $result9 = mysqli_query($connection, $query9);

    if ($result9) {
        $query4 = "INSERT INTO pro_releas_tbl(`releas_id`,`releas_quot_id`,`release_quot_det_id`,`releas_pro_name`,`pro_releas_qty`,`releas_date`,`releas_user`)VALUES(
                        0,
                        $id,
                        $detail_id,
                        '$name',
                        $releaseQty,
                        '$date',
                        $user)";

        $result4 = mysqli_query($connection, $query4);
        if (!$result4) {
            mysqli_rollback($connection);
            $response_array['status'] = 'error';
            echo json_encode($response_array);
            $ifSuccess = false;
            break;
        }
    }else{
        mysqli_rollback($connection);
        $response_array['status'] = 'error';
        echo json_encode($response_array);
        $ifSuccess = false;
    }
}

if ($ifSuccess) {
    mysqli_commit($connection);
    $response_array['status'] = 'success';
    echo json_encode($response_array);
}
?>