<?php
    include('../db/dbConnection.php');
?>
<?php
    session_start();

    $data = (array) $_POST['data'];

    $jobId = $data['jobId'];
    $response_array['quary'] = $data;
    $editQuoteId = $data['quoteId'];
    $cusId = $data['cusId'];
    $disc = $data['disc'];

    $date = date("m/d/Y");
    date_default_timezone_set("Asia/Colombo");
    $time = date("h:i:sa");
    $user = $_SESSION['user_id'];

    $select="SELECT MAX(invoice_id) AS max_id FROM invoice_tbl";
    $result= mysqli_query($connection,$select);
    $dataRow=mysqli_fetch_array($result);
    $dataRow = ++$dataRow['max_id'];

    $sql = mysqli_query($connection, "SELECT * FROM job_tbl,jobcategory_tbl WHERE job_tbl.job_id = '$jobId' AND jobcategory_tbl.jobCat_name = job_tbl.service_type");
    $res = mysqli_fetch_array($sql);

    $jobRef = $res['jobCat_subName'];
    $addTax = $data['addTax'];

    $year = date("Y");
    $invNo = "WEFIX/IN/".$year."/".$jobRef."/".$dataRow;

    $vat = $data['vat'];
    $check = $data['check'];
    $commercial = $data['commercial'];
    $cash=$data['cash'];

    $typeOfInvoice="";
    if ($vat>0){
        $typeOfInvoice="vat";
    }else if($check>0){
        $typeOfInvoice="check";
    }else if($commercial>0){
        $typeOfInvoice="commercial";
    }else if($cash>0){
        $typeOfInvoice="cash";
    }

    mysqli_autocommit($connection, false);

                    $query1 = "INSERT INTO invoice_tbl(`invoice_id`,`invoice_job`,`added_user`,`added_date`,`added_time`,`revise_invoice`,`reInvoice`,`invoice_no`,`quoteRef_id`,`invCus_id`,`inv_disc`,`payment`,`add_tax`,`check`,`vat`,`commercial`,`cash`)VALUES(
                        0,
                        $jobId,
                        $user,
                        '$date',
                        '$time',
                        0,
                        0,
                        '$invNo',
                        $editQuoteId,
                        $cusId,
                        $disc,
                        0,
                        $addTax,'$check','$vat','$commercial','$cash')";

                    $result1 = mysqli_query($connection, $query1);

                    if ($result1) {
                        if (!empty($data['proObj'])) {

                            for ($x = 0; $x < count($data['proObj']); $x++) {

                                $stockId = $data['proObj'][$x]['id'];
                                $proCode = $data['proObj'][$x]['code'];
                                $proName = $data['proObj'][$x]['proName'];
                                $price = $data['proObj'][$x]['price'];
                                $totQty = $data['proObj'][$x]['totQty'];
                                $description = $data['proObj'][$x]['desc'];

                                $query2 = "INSERT INTO invoice_details_tbl(`invoice_det_id`,`invoice_id`,`stock_id`,`pro_code`,`pro_name`,`totQty`,`invoice_desc`,`invoice_price`)VALUES(
                                    0,
                                    $dataRow,
                                    $stockId,
                                    '$proCode',
                                    '$proName',
                                    $totQty,
                                    '$description',
                                    $price)";

                                $result2 = mysqli_query($connection, $query2);

                                if (!$result2) {
                                    mysqli_rollback($connection);
                                    $response_array['status'] = 'error';
                                    echo json_encode($response_array);
                                }
                            }
                        }else {
                            mysqli_rollback($connection);
                            $response_array['status'] = 'error';
                            echo json_encode($response_array);
                        }
                        if (!empty($data['labourArray'])) {
                            for ($x = 0; $x < count($data['labourArray']); $x++) {

                                $packId = $data['labourArray'][$x]['ID'];

                                $query4 = "INSERT INTO invoice_lapack_tbl(`invLab_id`,`invPack_id`,`invoiceRef_id`)VALUES(
                                0,
                                $packId,
                                $dataRow)";

                                $result4 = mysqli_query($connection, $query4);

                                if (!$result4) {
                                    mysqli_rollback($connection);
                                    $response_array['status'] = 'error';
                                    echo json_encode($response_array);
                                }
                            }
                        }

                        $query6 = "UPDATE job_tbl SET 
                                        send_invoice='1',
                                        job_status='Send Invoice'
                                        WHERE 
                                        job_id='$jobId'";

                        $result6 = mysqli_query($connection, $query6);

                        if (!$result6) {
                            mysqli_rollback($connection);
                            $response_array['status'] = 'error';
                            echo json_encode($response_array);
                        }else {
                            if (!empty($data['additionalArray'])) {
                            for ($x = 0; $x < count($data['additionalArray']); $x++) {

                                $mainDesc = $data['additionalArray'][$x]['mainDesc'];
                                $subDesc = $data['additionalArray'][$x]['subDesc'];
                                $price = $data['additionalArray'][$x]['price'];

                                $query7 = "INSERT INTO invoice_additional_tbl(`additional_id`,`mainDec`,`subDesc`,`additional_price`,`invoice_id`)VALUES(
                                    0,
                                    '$mainDesc',
                                    '$subDesc',
                                    $price,
                                    $dataRow)";

                                $result7 = mysqli_query($connection, $query7);

                                if (!$result7) {
                                    mysqli_rollback($connection);
                                    $response_array['status'] = 'error';
                                    echo json_encode($response_array);
                                }
                            }
                        }
                        }

//                        $sql = mysqli_query($connection, "SELECT * FROM quatation_tbl WHERE quate_id = '$id'");
//                        $res = mysqli_fetch_array($sql);
//                        $QutNum = $res['quote_no'];

//                        $from = "kasunviduranga.kv@gmail.com";
//                        $number = 100;
//
//                        $headers = "From: $from";
//                        $headers = "From: " . $from . "\r\n";
//                        $headers .= "Reply-To: ". $from . "\r\n";
//                        $headers .= "MIME-Version: 1.0\r\n";
//                        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
//
//                        $subject = "You have a Quotation Accepted message from Wefix Software.";
//
//                        $logo = 'http://test.skyydigital.com/piumiTours/image/logo.jpg';
//                        $link = 'http://test.skyydigital.com/piumiTours/';
//
//                        $body = "<html lang='en'><body>";
//                        $body .= "<table style='width: 100%;'>";
//                        $body .= "<thead style='text-align: center;'><tr><td style='border:none;' colspan='2'>";
//                        $body .= "<a href='{$link}'><img src='{$logo}' alt=''></a><br><br>";
//                        $body .= "</td></tr></thead><tbody><tr>";
//                        $body .= "<td style='border:none;'><strong>Email:</strong> {$from}</td>";
//                        $body .= "</tr>";
//                        $body .= "<tr><td style='border:none;'><strong>Quotation Number:</strong> {$number}</td></tr>";
//                        $body .= "<tr><td></td></tr>";
//                        $body .= "</tbody></table>";
//                        $body .= "</body></html>";
//
//                        $sql="SELECT * From email_table";
//                        $result = mysqli_query($connection,$sql);
//                        while($dataRow=mysqli_fetch_assoc($result)){
//                            $to =$dataRow['mail'];
//                            $send = mail($to, $subject, $body, $headers);
//                        }
//


                        mysqli_commit($connection);
                        $response_array['pos_id'] = $dataRow;
                        $response_array['status'] = 'success';
                        $response_array['invoiceType'] = $typeOfInvoice;
                        echo json_encode($response_array);

                    }else {
                        mysqli_rollback($connection);
                        $response_array['status'] = 'error';
                        echo json_encode($response_array);
                    }
                

?>
