<?php
    include('../db/dbConnection.php');
?>
<?php
    session_start();

    $data = (array) $_POST['data'];

    $cusId = $data['cusId'];
    $jobId = $data['jobId'];
    $editQuoteId = $data['quoteId'];
    $quotNo = substr_replace($data['quoteNo'], 'R/', 9, 0);
    $disc = $data['disc'];
    $advance = $data['advance'];
    $addTax = $data['addTax'];

    $date = date("m/d/Y");
    date_default_timezone_set("Asia/Colombo");
    $time = date("h:i:sa");
    $user = $_SESSION['user_id'];
    

    $select="SELECT MAX(quate_id) AS max_id FROM quatation_tbl";
    $result= mysqli_query($connection,$select);
    $dataRow=mysqli_fetch_array($result);
    $dataRow = ++$dataRow['max_id'];

    mysqli_autocommit($connection, false);

                    $query1 = "INSERT INTO quatation_tbl(`quate_id`,`added_user`,`added_date`,`added_time`,`revise_quote`,`reQuote`,`quote_no`,`accept_qte`,`released_pro`,`quote_cusID`,`quote_disc`,`quote_advance`,`add_tax`,`quote_job`)VALUES(
                        '$dataRow',
                        '$user',
                        '$date',
                        '$time',
                        '0',
                        '1',
                        '$quotNo',
                        '0',
                        '0',
                        '$cusId',
                        '$disc',
                        '0',
                        '$addTax',
                        '$jobId')";

                    $result1 = mysqli_query($connection, $query1);

                    if ($result1) {
                        if (!empty($data['proObj'])) {

                            for ($x = 0; $x < count($data['proObj']); $x++) {

                                $stockId = $data['proObj'][$x]['id'];
                                $proCode = $data['proObj'][$x]['code'];
                                $proName = $data['proObj'][$x]['proName'];
                                $price = $data['proObj'][$x]['price'];
                                $totQty = $data['proObj'][$x]['totQty'];
                                $description = $data['proObj'][$x]['desc'];

                                $query2 = "INSERT INTO quate_details_tbl(`quate_det_id`,`quate_id`,`stock_id`,`pro_code`,`pro_name`,`totQty`,`quate_desc`,`quote_price`,`releas_qty`)VALUES(
                                    '',
                                    '$dataRow',
                                    '$stockId',
                                    '$proCode',
                                    '$proName',
                                    '$totQty',
                                    '$description',
                                    '$price',
                                    '$totQty')";

                                $result2 = mysqli_query($connection, $query2);

                                if (!$result2) {
                                    mysqli_rollback($connection);
                                    $response_array['status'] = 'error';
                                    echo json_encode($response_array);
                                }
                            }
                        }else {
                            mysqli_rollback($connection);
                            $response_array['status'] = 'error';
                            echo json_encode($response_array);
                        }

                        for ($x = 0; $x < count($data['labourArray']); $x++) {

                            $packId = $data['labourArray'][$x]['ID'];

                            $query4 = "INSERT INTO quate_lapack_tbl(`qutLab_id`,`quitPack_id`,`quateRef_id`)VALUES(
                                '',
                                '$packId',
                                '$dataRow')";

                            $result4 = mysqli_query($connection, $query4);

                            if (!$result4) {
                                mysqli_rollback($connection);
                                $response_array['status'] = 'error';
                                echo json_encode($response_array);
                                break;
                            }
                        }

                        if (!empty($data['additionalArray'])) {
                            for ($x = 0; $x < count($data['additionalArray']); $x++) {

                                $mainDesc = $data['additionalArray'][$x]['mainDesc'];
                                $subDesc = $data['additionalArray'][$x]['subDesc'];
                                $price = $data['additionalArray'][$x]['price'];

                                $query7 = "INSERT INTO quote_additional_tbl(`additional_id`,`mainDec`,`subDesc`,`additional_price`,`quote_id`)VALUES(
                                    '',
                                    '$mainDesc',
                                    '$subDesc',
                                    '$price',
                                    '$dataRow')";

                                $result7 = mysqli_query($connection, $query7);

                                if (!$result7) {
                                    mysqli_rollback($connection);
                                    $response_array['status'] = 'error';
                                    echo json_encode($response_array);
                                    break;
                                }
                            }
                        }

                        mysqli_commit($connection);
                        $response_array['pos_id'] = $dataRow;
                        $response_array['status'] = 'success';
                        echo json_encode($response_array);

                    }else {
                        mysqli_rollback($connection);
                        $response_array['status'] = 'error';
                        echo json_encode($response_array);
                    }
                

?>